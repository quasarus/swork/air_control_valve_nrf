#include "main.h"
#include "nrf_drv_rtc.h"
#include <time.h>
#ifndef __AIRDEVICE_CALENDAR
#define __AIRDEVICE_CALENDAR

#define CALENDAR_TIMER_FREQ_HZ 8
typedef struct
{
	bool IsSync;
	uint32_t SyncSecValue;
	uint32_t CurrentRtcOverflowValue;	
}AirDeviceCalendarTypeDef;



uint8_t GetCurrentDateTime(struct tm * timeinfo);
void SetCurrentDateTime(int sec);
void WakeUpTimerEventHandler(nrf_drv_rtc_int_type_t int_type);
void CalendatTimerEventHandler(nrf_drv_rtc_int_type_t int_type);
void HandleSetDateTimeCommand(uint8_t * data, uint32_t len);
void HandleGetDateTimeCommand(void);

bool IsDateTimeSynchronized(void);
uint32_t GetCurrentTimeSec(void);
#endif

