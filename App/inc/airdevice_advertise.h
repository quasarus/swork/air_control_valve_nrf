#ifndef __AIRDEVICE_ADVERTISE
#define __AIRDEVICE_ADVERTISE

#include "airdevice_eeprom.h"
#include "main.h"


#define NO_COMPANY_MANUF_DATA_ID 0xFFFF

#define ADV_DAT_FLAG_LOW_BATTERY (1<<0)
#define ADV_DAT_FLAG_BASE_DEV_VISIBLE (1<<1)

#define APP_ADV_INTERVAL                4800                                          /**< The advertising interval (in units of 0.625 ms. This value corresponds to 40 ms). */
#define APP_ADV_DURATION                0                                       		/**< The advertising duration (180 seconds) in units of 10 milliseconds. */
#define APP_BLE_CONN_CFG_TAG            1                                           /**< A tag identifying the SoftDevice BLE configuration. */

void AdvertisingInit(AirDeviceConfigTypeDef *config);
void AdvertiseStart(void);
void UpdateAdvData(void);
void UpdateRespData(AirDeviceConfigTypeDef *config);
void SetBaseDeviceVisibility(bool isVisible);
void HandleSetDeviceNameCommand(uint8_t * data, uint32_t len, AirDeviceConfigTypeDef *config);
#endif

