#include "main.h"
#include "FreeRtos.h"
#include "semphr.h"
#include "airdevice_journal.h"
#ifndef __AIRDEVICE_QUEUE
#define __AIRDEVICE_QUEUE
#include "airdevice_alg1.h"
#define QUEUE_DATA_ITEM_SIZE (NRF_SDH_BLE_GATT_MAX_MTU_SIZE)

#define QUEUE_RX_MAX_LEN 10


#define QUEUE_RX_DATA_SIZE (QUEUE_DATA_ITEM_SIZE * QUEUE_RX_MAX_LEN)


#define DUMPER_POS_QUEUE_LEN 5

typedef struct 
{
	uint8_t *pItem;
	uint8_t ItemSize;
}BleQueueItemTypeDef;

typedef struct 
{
	QueueHandle_t RxEmptyItemsQueue;
	QueueHandle_t RxFullItemsQueue;
	ble_airdevice_t *pAirDevInstance;
}BleTransactDataTypeDef;

typedef int (*QueueItemHandlerTypeDef) (uint8_t * pItem, uint32_t itemLen);

void OsInit(void);
BaseType_t PutDataToRxQueue(const uint8_t *data,uint16_t len);
BaseType_t SendBleData(uint8_t *data,uint16_t len);
BaseType_t TryGetDataFromRxQueue(QueueItemHandlerTypeDef itemHandleCallBack);


extern QueueHandle_t DumperPosQueue;
extern SemaphoreHandle_t xSemaphoreHum;
void SuspendAllTasks(void);
void ResumeConnectionTasks(void);
void SuspendWakeUpTasks(void);
void ResumeWakeUpTasks(void);
void OnStartFirmawareUpdate(void);
void AddDumperPosToQueue(DumperPositionType position,JournalEventTriggerType trigger);
void ResetRxQueues(void);
#endif
