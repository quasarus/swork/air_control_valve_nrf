#include "main.h"
#include <time.h>

#ifndef __AIRDEVICE_JOURNAL
#define __AIRDEVICE_JOURNAL
#include "airdevice_calendar.h"

typedef enum
{
	JEVENT_TRIGGER_USER,
	JEVENT_TRIGGER_SENSOR,
	JEVENT_TRIGGER_BASE,
	JEVENT_TRIGGER_SELF
}JournalEventTriggerType;

typedef enum
{
	JEVENT_MOVE_1_6,
	JEVENT_MOVE_2_6,
	JEVENT_MOVE_3_6,
	JEVENT_MOVE_4_6,
	JEVENT_MOVE_5_6,
	JEVENT_MOVE_FULL_CLOSE,
	JEVENT_MOVE_FULL_OPEN,
	JEVENT_FAULT_MOVE_TIMEOUT
}JournalEventType;


typedef struct
{
	uint32_t Time;
	JournalEventTriggerType Trigger;
	JournalEventType Event;	
}JournalEventItemTypeDef;

typedef struct
{
	uint32_t Time;
	uint8_t Trigger;
	uint8_t Event;	
}JournalEventItemType;
//#if sizeof(JournalEventType) != 1 || sizeof(JournalEventItemTypeDef) != 1
//#error "Wrong sizeof JournalEventType and JournalEventItemTypeDef"
//#endif


typedef struct
{	
	uint32_t Time;
	float Temperature;
	float Humidity;	
}JournalMeasureItemTypeDef;

void SaveEventToJournal(JournalEventTriggerType trigger, JournalEventType event);
void SaveMeasuresToJournal(float temperature, float humidity);
void HandleSetEnDisJournalCommand(AirDeviceConfigTypeDef *config,uint8_t * data, uint32_t len);
void HandleGetEnDisJournalCommand(AirDeviceConfigTypeDef *config);
void HandleGetJournalEventsCommand(uint8_t * data, uint32_t len);
void HandleGetJournalMeasuresCommand(uint8_t * data, uint32_t len);
#endif

