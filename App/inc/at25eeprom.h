#ifndef _W25QXX_H
#define _W25QXX_H

#ifdef __cplusplus
 extern "C" {
#endif

#include <stdbool.h>
#include "main.h"
#include "nrf_drv_spi.h"
#include "airdevice_eeprom.h"
	 
#define SPI_DUMMY_BYTE 0xA5	
//#define W25QXX_PAGE_SIZE 0x100
//#define W25QXX_SECTOR_SIZE 0x1000
	 
#define NRF52_MAX_SPI_TRANSFER_LEN 0xFF
	

#define AT25_PAGE_SIZE 64	 
#define AT25256_PAGE_NUM 512
	 
#define AT25_SIZE_BYTES 0x8000  
	 
#define AT25256_CONFIG_AREA_PAGE_NUM 16
#define AT25256_DATA_COUNTERS_AREA_PAGE_NUM 4
#define AT25256_EVENT_AREA_PAGE_NUM 246
#define AT25256_MEASURE_AREA_PAGE_NUM 246
	 
#define AT25256_CONFIG_AREA_OFFSET 0
#define AT25256_DATA_COUNTERS_AREA_OFFSET (AT25256_CONFIG_AREA_OFFSET + (AT25256_CONFIG_AREA_PAGE_NUM * AT25_PAGE_SIZE))//
#define AT25256_EVENT_AREA_OFFSET (AT25256_DATA_COUNTERS_AREA_OFFSET + (AT25256_DATA_COUNTERS_AREA_PAGE_NUM * AT25_PAGE_SIZE))
#define AT25256_MEASURE_AREA_OFFSET (AT25256_EVENT_AREA_OFFSET + (AT25256_EVENT_AREA_PAGE_NUM * AT25_PAGE_SIZE))

#define AT25256_END_OFFSET (AT25256_MEASURE_AREA_OFFSET + (AT25256_MEASURE_AREA_PAGE_NUM * AT25_PAGE_SIZE))

#define MAX_SAVED_EVENTS_NUM (uint32_t)((AT25256_EVENT_AREA_PAGE_NUM * AT25_PAGE_SIZE)/(sizeof(JournalEventItemTypeDef)-2))
#define MAX_SAVED_MEASURES_NUM (uint32_t)((AT25256_MEASURE_AREA_PAGE_NUM * AT25_PAGE_SIZE)/sizeof(JournalMeasureItemTypeDef))

#if ((AT25256_CONFIG_AREA_PAGE_NUM + AT25256_EVENT_AREA_PAGE_NUM + AT25256_DATA_COUNTERS_AREA_PAGE_NUM + AT25256_MEASURE_AREA_PAGE_NUM) != AT25256_PAGE_NUM)
#error "Wrong EEPROM Mem divide"
#endif

#if (AT25256_END_OFFSET != AT25_SIZE_BYTES)
#error "Wrong AT25256 EEPROM Mem areas calculation"
#endif

#define AT25_WREN_EEPROM_CMD 0x06
#define AT25_WRDI_EEPROM_CMD 0x04
#define AT25_RDSR_EEPROM_CMD 0x05
#define AT25_WRSR_EEPROM_CMD 0x01
#define AT25_READ_EEPROM_CMD 0x03
#define AT25_WRITE_EEPROM_CMD 0x02

void SpiEepromEventHandle(nrf_drv_spi_evt_t const * p_event,void *p_context);

//void At25RdPage(uint8_t *pBuffer,uint32_t address,uint32_t NumByteToRead);
//void At25WrPage(uint8_t *pBuffer,uint32_t address,uint32_t NumBytes);
uint8_t At25CompareWriteData(uint32_t eeprom_Address,uint8_t *pBuffer,uint32_t len);
void At25Rd(uint8_t *pBuffer,uint32_t address,uint32_t NumBytes);
void At25Wr(uint8_t *pBuffer,uint32_t address,uint32_t NumBytes);
void At25EnterUltraDeepSleep(void);

#ifdef __cplusplus
}
#endif

#endif


