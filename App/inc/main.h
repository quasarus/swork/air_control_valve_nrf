#ifndef __MAIN_H
#define __MAIN_H

#include <stdint.h>
#include <string.h>



#ifndef BLOADER

#include "FreeRTOS.h"
#include "queue.h"
#include "task.h"
#include "timers.h"

//#define RTT_DEBUG

#define ADV_ID_LEN 6
#define DEVICE_NAME_MAX_LEN 16
#define DEVICE_NAME_MIN_LEN 8
#define DEFAULT_DEVICE_NAME                     "AirValve" 

#define ENCODER_EXISTS

#define ALG_DEMO



#include "nordic_common.h"
#include "nrf.h"
#include "ble_hci.h"
#include "ble_advdata.h"
#include "ble_advertising.h"
#include "ble_conn_params.h"
#include "nrf_sdh.h"
#include "nrf_sdh_soc.h"
#include "nrf_sdh_ble.h"
#include "nrf_ble_gatt.h"
#include "nrf_ble_qwr.h"
#include "app_timer.h"
#include "airdevice_service.h"
#include "app_uart.h"
#include "app_util_platform.h"
#include "nrf_pwr_mgmt.h"
#include "nrf_drv_clock.h"
#include "nrf_drv_spi.h"
#include "nrfx_spim.h"
#include "nrf_drv_spi.h"
#include "nrfx_saadc.h"
#include "nrf_drv_saadc.h"
#include "nrf_drv_pwm.h"
#include "nrfx_pwm.h"
#include "nrf_ble_scan.h"
//#include "sdk_macros.h"
#include "nrf_fstorage.h"
//#include "peer_manager_types.h"
#include "peer_manager.h"
#include "peer_manager_handler.h"
//#include "ble_db_discovery.h"
#include "ble_gap.h"
#include "airdevice_calendar.h"

extern volatile bool IsConnected;

typedef struct
{
	uint8_t DeviceNameLen;
	char DeviceName[DEVICE_NAME_MAX_LEN];
	uint8_t AdvId[ADV_ID_LEN];
	uint16_t WakeUpTimeSec;
	bool IsDumperCalibrated;
	double DumperFullOpenAdcMul;
	double DumperFullCloseAdcMul;
	bool IsJournalEnabled;
}AirDeviceConfigTypeDef;

#include "airdevice_init.h"

#include "airdevice_advertise.h"
#include "airdevice_eeprom.h"
#include "airdevice_evt_ble.h"
#include "airdevice_dumper.h"
#include "airdevice_hum_sensor.h"
#include "airdevice_queue.h"
#include "airdevice_service.h"
#include "airdevice_adc.h"
#include "airdevice_alg1.h"
#include "airdevice_journal.h"

#include "at25eeprom.h"
#include "nrfx_twi.h"
#include "nrfx_twim.h"
#include "nrf_drv_twi.h"


#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"
#else
#include "app_error.h"
#endif

#if defined (UART_PRESENT)
#include "nrf_uart.h"
#endif
#if defined (UARTE_PRESENT)
#include "nrf_uarte.h"
#endif





                              
#define AIRDEVICE_BASE_UUID_ADV                  {{0x01,0x06,0x00,0x02,0x3F,0x51,0x86,0x0F,0xA8,0x81,0x71,0x45,0x5B,0x8C,0x5B,0xF4}}
#define BLE_UUID_AIRDEVICE_SERVICE_ADV 0xA55B /**< The UUID of the Nordic UART Service. */
#define AIRDEVICE_COMPANY_ID_ADV 0x000D

#define AIRDEVICE_SERVICE_UUID 0xADE0
#define AIRDEVICE_MAIN_CHARACTERISTIC_UUID (AIRDEVICE_SERVICE_UUID)
#define AIRDEVICE_HUM_CHARACTERISTIC_UUID (AIRDEVICE_MAIN_CHARACTERISTIC_UUID + 1)

#define AIRDEVICE_SERVICE_UUID_128                  {{0x01,0x06,0x00,0x02,0x3F,0x51,0x86,0x0F,0xA8,0x81,0x71,0x45,0x5B,0x8C,0x5B,0xF4}}

#define AIRDEVICE_DEFAULT_NAME_UUID_128                  {{0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x10,0x11,0x12,0x13,0x14,0x15,0x16}}

#define BLE_ADV_ID_BYTE1 0x19
#define BLE_ADV_ID_BYTE2 0x89
//#define BLE_ADV_ID_BYTE3 0xA9
#define BLE_ADV_ID_BYTE_BASE_DEVICE 0x24
#define BLE_ADV_ID_BYTE_VALVE_DEVICE 0x23

#define AIRDEV_SERVICE_DATA_ID_1 (((uint8_t)BLE_ADV_ID_BYTE2 << 8) | (uint8_t)BLE_ADV_ID_BYTE1)
#define AIRDEV_SERVICE_DATA_ID_2 (((uint8_t)BLE_ADV_ID_BYTE_VALVE_DEVICE << 8) | (uint8_t)BLE_ADV_ID_BYTE3)


#define BLE_ADV_BASE_DATA_LEN 12





#ifndef BLOADER
extern BleTransactDataTypeDef BleTransactData;
uint16_t GetConnectionHandle(void);
uint16_t GetMaxDataLen(void);


//uint32_t GetSysTick();

#endif



#endif

