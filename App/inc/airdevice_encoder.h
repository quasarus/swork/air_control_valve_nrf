#ifndef __AIRDEVICE_ENCODER
#define __AIRDEVICE_ENCODER

#include "main.h"
#define MAX_WR_PER_OPERATION_SIZE 8

#define AS5600_RAW_ANGLE_ADDRESS 0x0C
#define AS5600_CONF_ADDRESS 0x07
#define AS5600_BURN_ADDRESS 0xFF
#define AS5600_BURN_SETTINGS_COMMAND 0x40

#define AS5600_CONF_LPM2_MASK 0x02
#define AS5600_CONF_HYST_2LSB_MASK 0x08


void EncoderConfig(void);

ret_code_t GetRowAngle(uint16_t *rowAngle);
#endif
