#ifndef __AIRDEVICE_PWM
#define __AIRDEVICE_PWM

#include "main.h"

void StartMotor(bool isCw);
void StopMotor(void);

#endif
