#ifndef __AIRDEVICE_LCD
#define __AIRDEVICE_LCD
#include "main.h"
#include "airdevice_queue.h"
#include "airdevice_pwm.h"
#include "nrf_gpio.h"
#include "public.h"

void HandleDumperPositionSet(BleTransactDataTypeDef *transactData,uint8_t pos);
void HandleGetFwVersionCommand(void);

#endif
