#ifndef __AIRDEVICE_ADC
#define __AIRDEVICE_ADC
#include "airdevice_adc.h"
#define POW_MEASURE_NUM 5
#define POW_MEASURE_INTERVAL 3000
#define POW_CONV_INTERVAL 1


#define ADC_VCC_DIVIDER_VAL 2
#define ADC_REF_VOLTAGE 0.6
#define ADC_MAX_VALUE 0x0FFF
#define ADC_VCC_DIV 4
#define ADC_ENC_DIV 6

#define ADC_VCC_VOLTAGE_PER_ADC_VAL (((ADC_REF_VOLTAGE * ADC_VCC_DIV)/ADC_MAX_VALUE)*ADC_VCC_DIVIDER_VAL)
#define ADC_ENC_VOLTAGE_PER_ADC_VAL (((ADC_REF_VOLTAGE * ADC_ENC_DIV)/ADC_MAX_VALUE))

#define BATTERY_LOW_THR_VOLTAGE (double)2.0
	
#define ADC_VCC_CHANNEL 0
#define ADC_ENCODER_CHANNEL 7

#define ADC_ENCODER_EXTREME_POS_MEAS_NUM 5
#define ADC_ENCODER_EXTREME_ABS_VAL 16

#define DUMPER_GAP_MUL_FULL_OPEN (double)0.05
#define DUMPER_GAP_MUL_FULL_CLOSE (double)0.05

void saadc_callback(nrf_drv_saadc_evt_t const * p_event);
void adc_task_function (void * pvParameter);
bool IsBatteryLow(void);
double GetAveragedVccVoltage(void);
nrf_saadc_value_t GetVccAdcVal(void);
nrf_saadc_value_t GetEncoderAdcVal(void);
void CheckBatteryVoltage(void);
double MeasureEncoderAdcValWhileChanged(void);
void GetAveragedVccAndEncVoltage(double *vccVoltage,double *encVoltage);
#endif
