#include "main.h"
#include "nrf_timer.h"
#ifndef __AIRDEVICE_COMMON_TIMERS
#define __AIRDEVICE_COMMON_TIMERS

void TimeoutTimerEventHandler(nrf_timer_event_t event_type, void* p_context);
void StartTimeoutTimer(uint32_t secDelay);
void StopTimeoutTimer(void);
bool CheckTimeout(void);
#endif

