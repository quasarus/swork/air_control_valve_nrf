#ifndef __AIRDEVICE_EEPROM
#define __AIRDEVICE_EEPROM

#ifndef BLOADER

#include "nrf_delay.h"
#include "at25eeprom.h"
#include "airdevice_init.h"
#include "crc32_calc.h"
#include "main.h"
#include "version.h"
#include "nrf_fstorage.h"
#include "nrf_nvic.h"
#include "public.h"
#include "nrf_drv_rng.h"


#endif 

#define CONFIG_SECTOR_START_ADDRESS 0
#define FIRMWARE_SECTOR_START_ADDRESS (CONFIG_SECTOR_START_ADDRESS + W25QXX_SECTOR_SIZE)
#define SECTOR_SIZE W25QXX_SECTOR_SIZE
#define BLE_DATA_CHUNK_SIZE (BLE_GATT_ATT_MTU_DEFAULT - 3 - sizeof(uint8_t))
#define EEPROM_OP_ATTEMPTS_NUM 4
//bool IsEepromFirmwareExists(FirmwareInfoTypeDef *fwInfo);
void GetEepromFirmwareInfo(FirmwareInfoTypeDef *fwInfo);
uint8_t ReadAirDeviceConfigFromEEPROM(AirDeviceConfigTypeDef *config);
uint8_t SaveAirDeviceConfigToEEPROM(AirDeviceConfigTypeDef *config);

void EEPROMEnterUltraDeepSleep(void);
void PrepareEepromWrite(uint8_t * data, uint32_t Len);
void HandleEndWriteCommand(void);
void HandleWriteCommand(uint8_t * data, uint32_t Len);
void CheckFwUpdate(void);
uint8_t CheckEepromFirmwareIntegrity(void);

void fstorage_evt_handler(nrf_fstorage_evt_t *p_evt);

extern AirDeviceConfigTypeDef AirDeviceConfig;
extern FirmwareInfoTypeDef FirmwareInfo;

#endif
