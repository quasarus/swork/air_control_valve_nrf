
#ifndef __AIRDEVICE_INIT
#define __AIRDEVICE_INIT


#include "main.h"
#include "nrf_gpio.h"
#include "at25eeprom.h"
#include "nrf_drv_ppi.h"
#include "nrf_drv_gpiote.h"
#include "nrfx_ppi.h"
#include "version.h"
#include "nrf_delay.h"
#include "nrf_twi.h"
#include "nrf_twim.h"
#include "nrf_drv_twi.h"
#include "airdevice_eeprom.h"
#include "nrf_fstorage.h"
#include "nrf_drv_timer.h"
#include "nrf_drv_rtc.h"
#include "nrf_drv_clock.h"
//#define IS_ENCODER_PRESENT
#include "airdevice_common_timers.h"
#include "app_timer.h"
#include "nrf_drv_clock.h"
#define HTU21_TWI_ADDRESS 0x40
#define HTU21_TWI_COMMAND_TEMP_MEAS_NO_HOLD 0xF3
#define HTU21_TWI_COMMAND_HUM_MEAS_NO_HOLD 0xF5

#define AS5600_TWI_ADDRESS 0x36

#define HUM_SENSOR_SCL_PIN 21
#define HUM_SENSOR_SDA_PIN 20
#define HUM_SENSOR_ADDR_PIN 7

#define MOTOR_TACT_DURATION_MS 5000		//5000

#define EEPROM_SCK_PIN 3
#define EEPROM_MISO_PIN 9
#define EEPROM_MOSI_PIN 4
#define EEPROM_CS_PIN 10

#define BUTTON_PIN 24
#define LED_IND_PIN 7

#define ENC_POWER_EN_PIN 26
#define VCC_T_EN_PIN 27
#define EN_5V_PIN 30
#define DC5V_EN_PIN 13	//31
#define ADC_EN_PIN 5


#define BATTERY_MEAS_SAADC_INPUT NRF_SAADC_INPUT_AIN0
#define ENCODER_SAADC_INPUT NRF_SAADC_INPUT_AIN7


#define MOTOR_PWM_PIN_A 19		//��� 28BYJ-48 ���������
#define MOTOR_PWM_PIN_B 18		//��� 28BYJ-48 ������
#define MOTOR_PWM_PIN_C 28		//��� 28BYJ-48 �������
#define MOTOR_PWM_PIN_D 29		//��� 28BYJ-48 �����


#define ENCODER_HUM_TWI_INSTANCE_ID 0
//#define HUM_SENSOR_TWI_INSTANCE_ID 1
#define EEPROM_SPI_INSTANCE_ID 2

//#define APP_ROM_START_ADDRESS 0x00026000




#if defined(SOFTDEVICE_PRESENT)
#include "nrf_fstorage_sd.h"
#else
#include "nrf_fstorage_nvmc.h"
#endif
#include "public.h"
//#define APP_MEM_START_ADDR 0x26000
//#define APP_MEM_LEN 0x5A000


#define USED_PWM(idx) (1UL << idx)

void GpioInit(void);

void SpiInit(void);

void TimersInit(void);

void AdcInit(void);

void PwmInit(void);

void TwiInit(void);

void FstorageInit(void);

void WakeUpTimerInit(void);
void CalendarTimerInit(void);
//void WakeUpTimerInit(void);

extern const nrf_drv_spi_t spi_eeprom;
extern nrf_ppi_channel_t ppi_channel;
extern nrf_drv_pwm_t m_pwm0;
extern const nrf_drv_twi_t hum_enc_twi;
extern nrf_fstorage_t fstorage;
//extern const nrf_drv_rtc_t wake_up_rtc;
extern const nrf_drv_timer_t timeout_timer;
extern const nrf_drv_rtc_t calendar_rtc;
#endif
