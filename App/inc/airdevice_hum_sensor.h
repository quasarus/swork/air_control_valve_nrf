#ifndef __AIRDEVICE_HUM_SENSOR
#define __AIRDEVICE_HUM_SENSOR
#include "main.h"
#include "nrf_drv_twi.h"
#include "airdevice_encoder.h"

#define HUM_MEASURE_DATA_LEN 3
#define TWI_ATTEMPT_NUM 500
#define HUM_MEASURE_ATTEMPT_NUM TWI_ATTEMPT_NUM
void enc_hum_handler(nrf_drv_twi_evt_t const * p_event, void * p_context);
ret_code_t GetTemperatureAndHumidity(float *temperature, float *humidity);

bool IsTwiXferDone(void);

bool IsAckOnTwi(void);

void ClearXferFlags(void);

uint8_t HandleGetTemperstureAndHumidityCommand(uint8_t * pItem, uint32_t itemLen);

#endif
