#ifndef __CRC32_CALC_H
#define __CRC32_CALC_H

#include "main.h"

uint32_t CalculateCRC32(const void *buf, size_t size);
extern const uint32_t crc32_tab[];
#endif

