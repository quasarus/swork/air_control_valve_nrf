#include "main.h"
#include "nrf_drv_timer.h"
#include "nrf_drv_rtc.h"
#include "nrf_drv_clock.h"
#include "airdevice_init.h"
#include "airdevice_journal.h"
#ifndef __AIRDEVICE_ALG1_H
#define __AIRDEVICE_ALG1_H
#include "airdevice_queue.h"
#include "math.h"

typedef enum{
	DUMPER_POS_IDLE,
	DUMPER_POS_INVALID,
	DUMPER_POS_CALIBRATION,
	DUMPER_POS_FULL_OPEN,
	DUMPER_POS_FULL_CLOSE,
	DUMPER_POS_PARTLY_1_6,
	DUMPER_POS_PARTLY_2_6,
	DUMPER_POS_PARTLY_3_6,
	DUMPER_POS_PARTLY_4_6,
	DUMPER_POS_PARTLY_5_6,	
}DumperPositionType;

typedef struct{
	DumperPositionType PosType;
	JournalEventTriggerType Trigger;
}DumperPositionItemTypeDef;

typedef enum{
	DUMPER_ERROR_NONE,
	DUMPER_ERROR_UNKNOWN,
	DUMPER_ERROR_CALIBRATION,
}DumperError;

//typedef enum{
//	DUMPER_STATE_IDLE,
//	DUMPER_STATE_INVALID,
//	DUMPER_STATE_CALIBRATION,
//	DUMPER_STATE_FULL_OPEN,
//	DUMPER_STATE_FULL_CLOSE,
//	DUMPER_STATE_HALF_OPEN,
//}DumperPositionType;


#define SAVE_MEASURE_INTERVAL 2

#define DUMPER_MOVE_TIMEOUT_SEC 7

#define HUM_TEMP_MEASURE_NUM 10
#define HUM_THR_FOR_OPEN_PERCENT 2
#define ADS_DIF_THR 0.02
//#define HUM_TEMP_POS_FOR_COMPARE 120

//#if HUM_TEMP_POS_FOR_COMPARE > HUM_TEMP_MEASURE_NUM
//#error "HUM_TEMP_POS_FOR_COMPARE can't be more than HUM_TEMP_MEASURE_NUM"
//#endif

void OnDeviceConnect(void);
void OnDeviceDisconnect(void);
void WakeUpTimerEventHandler(nrf_drv_rtc_int_type_t int_type);
void HandleWakeUpTask(AirDeviceConfigTypeDef *config);
void HandleSetWakeUpIntervalCommand(uint8_t * data, uint32_t len, AirDeviceConfigTypeDef *config);
void HandleGetWakeUpIntervalCommand(AirDeviceConfigTypeDef *config);
void dumper_task_function(void * pvParameter);
void HandleDumperCalibrationCommand(JournalEventTriggerType trigger);
#endif
