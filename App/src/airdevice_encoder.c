#include "airdevice_encoder.h"

static volatile bool m_xfer_done = false;
static volatile bool isAckOnTwi = false;

static ret_code_t ReadAS5600(uint8_t *rdRegs,uint8_t startRegisterAddr,uint8_t regsNum);
static ret_code_t WriteAS5600(uint8_t *wrRegs,uint8_t startRegisterAddr,uint8_t regsNum);
uint8_t twiTxBuf[MAX_WR_PER_OPERATION_SIZE + sizeof(uint8_t)]= {0};

ret_code_t GetRowAngle(uint16_t *rowAngle)
{
	uint8_t rawAngleBytes[2] = {0};
	ret_code_t result = ReadAS5600(rawAngleBytes, AS5600_RAW_ANGLE_ADDRESS,sizeof(rawAngleBytes));
	if(result != NRF_SUCCESS)
	{
		return result;
	}
	
	uint16_t rangle=0;
	rangle |= rawAngleBytes[0];
	rangle <<= 8;
	rangle |= rawAngleBytes[1];
	*rowAngle = rangle;
	return NRF_SUCCESS;
}

static ret_code_t ReadAS5600(uint8_t *rdRegs,uint8_t startRegisterAddr,uint8_t regsNum)
{
	uint8_t regAddress = startRegisterAddr;
	
	for(uint16_t att=0; att < TWI_ATTEMPT_NUM;att++)
	{
		ret_code_t err_code = nrf_drv_twi_tx(&hum_enc_twi, AS5600_TWI_ADDRESS, &regAddress, sizeof(uint8_t), false);
		APP_ERROR_CHECK(err_code);
		while (IsTwiXferDone() == false);
	
		if(!IsAckOnTwi())
		{
			ClearXferFlags();
			continue;			
		}
		
		ClearXferFlags();
	
		err_code = nrf_drv_twi_rx(&hum_enc_twi, AS5600_TWI_ADDRESS, rdRegs, regsNum);
		while (IsTwiXferDone() == false);
		ClearXferFlags();
		return NRF_SUCCESS;
	}		
	return NRF_ERROR_TIMEOUT;
}



static ret_code_t WriteAS5600(uint8_t *wrRegs,uint8_t startRegisterAddr,uint8_t regsNum)
{
	if(regsNum > (sizeof(twiTxBuf) - 1))
	{
		return NRF_ERROR_DATA_SIZE;
	}	
  
	twiTxBuf[0] = startRegisterAddr;
	memcpy(twiTxBuf + 1,wrRegs,regsNum);
	
	for(uint16_t att=0; att < TWI_ATTEMPT_NUM;att++)
	{
		ret_code_t err_code = nrf_drv_twi_tx(&hum_enc_twi, AS5600_TWI_ADDRESS, twiTxBuf, sizeof(uint8_t), false);
		APP_ERROR_CHECK(err_code);
		while (IsTwiXferDone() == false);
	
		if(!IsAckOnTwi())
		{
			ClearXferFlags();
			continue;			
		}
		
		ClearXferFlags();
		return NRF_SUCCESS;
	}		
	return NRF_ERROR_TIMEOUT;		
}

void EncoderConfig(void)
{
	uint8_t configBytesWr[2] = {0};
	uint8_t configBytesRd[2] = {0};
	configBytesWr[0] = 0x00;
	configBytesWr[1] = AS5600_CONF_HYST_2LSB_MASK | AS5600_CONF_LPM2_MASK;	

	WriteAS5600(configBytesWr,AS5600_CONF_ADDRESS,sizeof(configBytesRd));
	
	ReadAS5600(configBytesRd,AS5600_CONF_ADDRESS,sizeof(configBytesWr));
	if(configBytesWr[0] != configBytesRd[0] || configBytesWr[1] != configBytesRd[1])
	{
		nrf_delay_ms(5);
		uint8_t command = AS5600_BURN_SETTINGS_COMMAND;
		WriteAS5600(&command,AS5600_BURN_ADDRESS,sizeof(uint8_t));		
		nrf_delay_ms(5);
		uint8_t otpCommands[3] = {0};
		otpCommands[0] = 0x01;
		otpCommands[1] = 0x11;
		otpCommands[2] = 0x10;
		WriteAS5600(otpCommands,AS5600_BURN_ADDRESS,sizeof(otpCommands));
		ReadAS5600(configBytesRd,AS5600_CONF_ADDRESS,sizeof(configBytesRd));
		nrf_delay_ms(5);
	}
}

	