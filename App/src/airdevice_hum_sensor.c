#include "airdevice_hum_sensor.h"

static volatile bool m_xfer_done = false;
static volatile bool isAckOnTwi = false;

static uint8_t GetCrc(uint8_t *data, size_t len);

static const uint8_t measureCommand[] = {0xF3,0x00};

//static uint8_t rxTwiData[HUM_MEASURE_DATA_LEN]={0};
static ret_code_t MeasureRawTemperatureOrHumidity(bool isTMeasTemperature, uint16_t *data);

static uint8_t TxBleData[2 + sizeof(float)*2];

static ret_code_t MeasureRawTemperatureOrHumidity(bool isTMeasTemperature, uint16_t *data)
{
	ClearXferFlags();
	//nrf_delay_ms(50);
	uint8_t command = isTMeasTemperature ? HTU21_TWI_COMMAND_TEMP_MEAS_NO_HOLD : HTU21_TWI_COMMAND_HUM_MEAS_NO_HOLD;
	
  ret_code_t err_code = nrf_drv_twi_tx(&hum_enc_twi, HTU21_TWI_ADDRESS, &command, sizeof(uint8_t), false);
  APP_ERROR_CHECK(err_code);
  while (IsTwiXferDone() == false);
	
	if(!IsAckOnTwi())
  {
		ClearXferFlags();
		return NRF_ERROR_INTERNAL;
	}
		
  ClearXferFlags();		

	nrf_delay_ms(50);
	
	uint8_t twiData[HUM_MEASURE_DATA_LEN]={0};
	
	for(uint16_t att=0; att < TWI_ATTEMPT_NUM;att++)
	{
		ret_code_t err_code = nrf_drv_twi_rx(&hum_enc_twi, HTU21_TWI_ADDRESS, twiData, sizeof(twiData));
		while (IsTwiXferDone() == false);
		if(!IsAckOnTwi())
		{
			ClearXferFlags();
			continue;
		}
		ClearXferFlags();
			
		if(err_code == 0 && GetCrc(twiData,2) == twiData[2])
		{
			uint16_t rawData = 0;
			rawData = twiData[0];
			rawData <<= 8;
			uint8_t status = (twiData[1] & 0x02);
			if((status > 0 && isTMeasTemperature) || (status == 0 && !isTMeasTemperature))
			{
				return NRF_ERROR_INVALID_DATA;
			}

			rawData |= twiData[1] & ~0x03;
			*data = rawData;
			return NRF_SUCCESS;
		}
	}
	return NRF_ERROR_TIMEOUT;
}



ret_code_t GetTemperatureAndHumidity(float *temperature, float *humidity)
{
  ret_code_t err_code;

	uint16_t rawTemperature;
	uint16_t rawHumidity;

	err_code = MeasureRawTemperatureOrHumidity(true, &rawTemperature);
	if(err_code != NRF_SUCCESS)
	{
		return err_code;
	}
	err_code = MeasureRawTemperatureOrHumidity(false, &rawHumidity);
	if(err_code != NRF_SUCCESS)
	{
		return err_code;
	}  
	
  *temperature = -46.85 + 175.72*(((float)rawTemperature)/65536);
	*humidity = -6 + 125*(((float)rawHumidity)/65536);
	

	return NRF_SUCCESS;		
}


static uint8_t GetCrc(uint8_t *data, size_t len)
{
    uint8_t crc = 0x00;
    size_t i, j;
    for (i = 0; i < len; i++) {
        crc ^= data[i];
        for (j = 0; j < 8; j++) {
            if ((crc & 0x80) != 0)
                crc = (uint8_t)((crc << 1) ^ 0x31);
            else
                crc <<= 1;
        }
    }
    return crc;
}


void enc_hum_handler(nrf_drv_twi_evt_t const * p_event, void * p_context)
{
    switch (p_event->type)
    {
        case NRF_DRV_TWI_EVT_DONE:					
					isAckOnTwi = true;           
          break;
        default:
            break;
    }		
		m_xfer_done = true;
}

bool IsTwiXferDone(void)
{
	return m_xfer_done;
}

bool IsAckOnTwi(void)
{
	return isAckOnTwi;
}

void ClearXferFlags(void)
{
	m_xfer_done = false;
	isAckOnTwi = false;
}

uint8_t HandleGetTemperstureAndHumidityCommand(uint8_t * pItem, uint32_t itemLen)
{
	float temperature,humidity;
	uint8_t measResult = GetTemperatureAndHumidity(&temperature,&humidity);
	
	TxBleData[0] = CMD_RESP_MARKER_TEMPERATURE_AND_HUMIDITY;
	TxBleData[1] = measResult == 0 ? CMD_RESP_OK : CMD_RESP_GET_TEMP_HUM_ERROR;	
	memcpy(&TxBleData[2],&temperature,sizeof(float));
	memcpy((&TxBleData[2]) + sizeof(float),&humidity,sizeof(float));
	SendBleData(TxBleData,sizeof(TxBleData));
	return 0;
}
