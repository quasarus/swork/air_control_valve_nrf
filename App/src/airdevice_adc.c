#include "main.h"
#include <math.h>
static nrf_saadc_value_t AverageAdcResult(nrf_saadc_value_t *convs,uint8_t convNum);
static double CalculateVccVoltage(nrf_saadc_value_t averageAdcVal);
static double CalculateEncoderVoltage(nrf_saadc_value_t averageAdcVal);
static volatile bool IsBatteryLowFlag = false;

static nrf_saadc_value_t adc_vcc_convs[POW_MEASURE_NUM];


void saadc_callback(nrf_drv_saadc_evt_t const * p_event)
{
	if (p_event->type == NRF_DRV_SAADC_EVT_DONE)
  {
		printf("");
  }
}



static nrf_saadc_value_t AverageAdcResult(nrf_saadc_value_t *convs,uint8_t convNum)
{
	uint32_t sum = 0;
	for(uint8_t cnt = 0; cnt < convNum; cnt++)
	{
		sum += convs[cnt];
	}
	return sum/convNum;
}

static double CalculateVccVoltage(nrf_saadc_value_t averageAdcVal)
{
	return ADC_VCC_VOLTAGE_PER_ADC_VAL*(uint16_t)averageAdcVal;
}

static double CalculateEncoderVoltage(nrf_saadc_value_t averageAdcVal)
{
	return ADC_ENC_VOLTAGE_PER_ADC_VAL*(uint16_t)averageAdcVal;
}


void adc_task_function (void * pvParameter)
{
    UNUSED_PARAMETER(pvParameter);
		//BleTransactDataTypeDef *transactData = (BleTransactDataTypeDef*)pvParameter;

    while (true)
    {
			vTaskDelay(POW_MEASURE_INTERVAL);
			for(uint8_t cnt = 0; cnt < POW_MEASURE_NUM; cnt++)
			{
				ret_code_t err_code = nrf_drv_saadc_sample_convert(ADC_VCC_CHANNEL, &adc_vcc_convs[cnt]);

				vTaskDelay(POW_CONV_INTERVAL);				
			}
			nrf_saadc_value_t avr_adc_val = AverageAdcResult(adc_vcc_convs,POW_MEASURE_NUM);
			double voltage = CalculateVccVoltage(avr_adc_val);
			NRF_LOG_INFO("Voltage is %lf .",voltage);
			
			taskYIELD();
    }
}

double GetAveragedVccVoltage(void)
{
	for(uint8_t cnt = 0; cnt < POW_MEASURE_NUM; cnt++)
	{
		ret_code_t err_code = nrf_drv_saadc_sample_convert(ADC_VCC_CHANNEL, &adc_vcc_convs[cnt]);

		vTaskDelay(POW_CONV_INTERVAL);	
		//nrf_delay_ms(10);
	}
	nrf_saadc_value_t avr_adc_val = AverageAdcResult(adc_vcc_convs,POW_MEASURE_NUM);
	return CalculateVccVoltage(avr_adc_val);
}

void GetAveragedVccAndEncVoltage(double *vccVoltage,double *encVoltage)
{
	nrf_saadc_value_t adc_enc_convs[POW_MEASURE_NUM];
	for(uint8_t cnt = 0; cnt < POW_MEASURE_NUM; cnt++)
	{
		ret_code_t err_code = nrf_drv_saadc_sample_convert(ADC_VCC_CHANNEL, &adc_vcc_convs[cnt]);
		if(err_code != NRFX_SUCCESS)
		{
			NRF_LOG_ERROR("ADC ERROR!");
		}			
		err_code = nrf_drv_saadc_sample_convert(ADC_ENCODER_CHANNEL, &adc_enc_convs[cnt]);
		if(err_code != NRFX_SUCCESS)
		{
			NRF_LOG_ERROR("ADC ERROR!");
		}	
		//vTaskDelay(POW_CONV_INTERVAL);	
		//nrf_delay_ms(10);
	}
	nrf_saadc_value_t avr_adc_val_vcc = AverageAdcResult(adc_vcc_convs,POW_MEASURE_NUM);
	nrf_saadc_value_t avr_adc_val_enc = AverageAdcResult(adc_enc_convs,POW_MEASURE_NUM);
	*vccVoltage = CalculateVccVoltage(avr_adc_val_vcc);
	*encVoltage = CalculateEncoderVoltage(avr_adc_val_enc);
	//return CalculateVccVoltage(avr_adc_val);
}

void CheckBatteryVoltage(void)
{
	double voltage = GetAveragedVccVoltage();
	IsBatteryLowFlag = voltage < BATTERY_LOW_THR_VOLTAGE ? true : false;
}

bool IsBatteryLow(void)
{
	return IsBatteryLowFlag;
}

nrf_saadc_value_t GetVccAdcVal(void)
{
	nrf_saadc_value_t val;
	ret_code_t err_code = nrf_drv_saadc_sample_convert(ADC_VCC_CHANNEL, &val);	
  APP_ERROR_CHECK(err_code);
	return val;
}

nrf_saadc_value_t GetEncoderAdcVal(void)
{
	nrf_saadc_value_t val;
	ret_code_t err_code = nrf_drv_saadc_sample_convert(ADC_ENCODER_CHANNEL, &val);	
  APP_ERROR_CHECK(err_code);
	return val;
}


double MeasureEncoderAdcValWhileChanged(void)
{
	uint8_t pos = 0;
	nrf_saadc_value_t enc_values[ADC_ENCODER_EXTREME_POS_MEAS_NUM];
	nrf_saadc_value_t vcc_values[ADC_ENCODER_EXTREME_POS_MEAS_NUM];
	while(true)
	{
		enc_values[pos] = GetEncoderAdcVal();
		vcc_values[pos] = GetVccAdcVal();
		
		if(pos < ADC_ENCODER_EXTREME_POS_MEAS_NUM)
		{
			pos++;
		}
		else
		{
			pos = 0;
			bool isValuesEquals = true;
			for(uint8_t cnt = 0; cnt < ADC_ENCODER_EXTREME_POS_MEAS_NUM - 1; cnt++)
			{

				double absol = abs(enc_values[cnt] - enc_values[cnt+1]);
				
				if(absol > ADC_ENCODER_EXTREME_ABS_VAL)
				{
					isValuesEquals = false;
					break;
				}
			}
			if(isValuesEquals)
			{
				double enc_extreme_val = 0;
				double vcc_value = 0;
				//double voltage = 0;
				for(uint8_t cnt = 0; cnt < ADC_ENCODER_EXTREME_POS_MEAS_NUM; cnt++)
				{
					enc_extreme_val += enc_values[cnt];
					vcc_value += vcc_values[cnt];
				}
				//voltage = GetAveragedVccVoltage();
//				double average_enc_extreme_val = enc_extreme_val/ADC_ENCODER_EXTREME_POS_MEAS_NUM;
//				double average_vcc_value = vcc_value/ADC_ENCODER_EXTREME_POS_MEAS_NUM;
				volatile double enc_vlt = CalculateEncoderVoltage(enc_extreme_val/ADC_ENCODER_EXTREME_POS_MEAS_NUM);
				volatile double vcc_vlt = CalculateVccVoltage(vcc_value/ADC_ENCODER_EXTREME_POS_MEAS_NUM);
				return vcc_vlt/enc_vlt;
			}
		}
		vTaskDelay(100);
	}
	return 0;
}

