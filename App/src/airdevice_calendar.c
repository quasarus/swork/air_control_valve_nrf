#include "airdevice_calendar.h"

static AirDeviceCalendarTypeDef AirDeviceCalendar = 
{
	.IsSync = false,
	.CurrentRtcOverflowValue = 0,
	.SyncSecValue = 0
};


uint8_t GetCurrentDateTime(struct tm * timeinfo)
{
	if(!AirDeviceCalendar.IsSync)
	{
		return 1;
	}
	/*����� �������� �������� �������� RTC, ��������� �� ��� �������, �������� ������, ����������� �� ������������ � ������� ������������� �������*/
	time_t current_time = GetCurrentTimeSec();    
		
	timeinfo = localtime( &current_time );
	return 0;
}

uint32_t GetCurrentTimeSec(void)
{
	return (nrfx_rtc_counter_get(&calendar_rtc) / CALENDAR_TIMER_FREQ_HZ) + AirDeviceCalendar.CurrentRtcOverflowValue + AirDeviceCalendar.SyncSecValue;	  
}

//������������� �������
void SetCurrentDateTime(int sec)
{
	//������� ������� ���������, ��������� ��������
	nrf_drv_rtc_disable(&calendar_rtc);
	nrf_drv_rtc_counter_clear(&calendar_rtc);
//	
	AirDeviceCalendar.CurrentRtcOverflowValue = 0;
	AirDeviceCalendar.SyncSecValue = sec;
	
	//����� ������� ����� �������������
	nrfx_rtc_overflow_enable(&calendar_rtc,true);	
	nrf_drv_rtc_enable(&calendar_rtc);	
	AirDeviceCalendar.IsSync = true;
//	struct tm timeinfo;
//	GetCurrentDateTime(&timeinfo);
}

void CalendatTimerEventHandler(nrf_drv_rtc_int_type_t int_type)
{ 
	switch(int_type)
	{
		case NRFX_RTC_INT_OVERFLOW:
		{
			//��������� �� ������������ �������� ������. �.� ������� ������� 8��, ����������� �������� 24��� ��������� �� �������� = (0xFFFFFF + 1)/8
			AirDeviceCalendar.CurrentRtcOverflowValue += 0x200000;
			break;
		}
		case NRFX_RTC_INT_TICK:
		{
			break;
		}
		case NRFX_RTC_INT_COMPARE0:
		{
			break;
		}	
		case NRFX_RTC_INT_COMPARE1:
		{
			break;
		}
		case NRFX_RTC_INT_COMPARE2:
		{
			break;
		}
		case NRFX_RTC_INT_COMPARE3:
		{
			break;
		}			
	}
}

void HandleSetDateTimeCommand(uint8_t * data, uint32_t len)
{
	if(len != sizeof(uint32_t))
	{
		return;		
	}
	
	SetCurrentDateTime(*((uint32_t*)data));
	
	uint8_t txBleBuf[2] = {(uint8_t)CMD_RESP_MARKER_DATETIME, (uint8_t)CMD_RESP_OK};
	SendBleData(txBleBuf,2);	
}

void HandleGetDateTimeCommand(void)
{	
	uint8_t txBleBuf[5];
	time_t current_time = GetCurrentTimeSec();
	txBleBuf[0] = (uint8_t)CMD_RESP_MARKER_DATETIME;
	txBleBuf[1] = (uint8_t)current_time;
	txBleBuf[2] = (uint8_t)(current_time >> 8);
	txBleBuf[3] = (uint8_t)(current_time >> 16);
	txBleBuf[4] = (uint8_t)(current_time >> 24);
	SendBleData(txBleBuf,5);
}

bool IsDateTimeSynchronized(void)
{
	return AirDeviceCalendar.IsSync;
}
