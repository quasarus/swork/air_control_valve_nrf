#include "airdevice_eeprom.h"
#include "public.h"


static volatile bool isErased = false;
static volatile bool isWrited = false;
static volatile ret_code_t EraseResult = NRF_SUCCESS;
static volatile ret_code_t WriteResult = NRF_SUCCESS;
static uint32_t random_vector_generate(uint8_t * p_buff, uint8_t size);
//static uint8_t random_vector_generate(uint8_t * p_buff, uint8_t size);
 
FirmwareInfoTypeDef FirmwareInfo __attribute__((at(APP_MEMORY_ADDRESS + FIRMWARE_INFO_OFFSET))) = 
{
	.Marker1 = FW_INFO_MARKER_1,
	.Marker2 = FW_INFO_MARKER_2,
	.Marker3 = FW_INFO_MARKER_3,
	.Version = FW_APP_VERSION
};

AirDeviceConfigTypeDef AirDeviceConfig;

static void HandleEepromWriteData(uint8_t* data, uint16_t len);
static void GetDefaultAirDeviceConfig(AirDeviceConfigTypeDef *config);

static uint32_t FileOffset;
//static uint8_t storedCrc[4] = {0x44,0x44,0x44,0x44};
//static uint8_t wr[0xFF] = {0};
//static uint8_t rd[0xFF] = {0};
uint8_t ReadAirDeviceConfigFromEEPROM(AirDeviceConfigTypeDef *config)
{

	//����������� ������ � EEPROM
	for(uint8_t cnt=0;cnt < EEPROM_OP_ATTEMPTS_NUM;cnt++)
	{
		uint32_t storedCrc;
		At25Rd((uint8_t*)config,CONFIG_SECTOR_START_ADDRESS + sizeof(uint32_t),sizeof(AirDeviceConfigTypeDef));
		
		At25Rd((uint8_t*)&storedCrc,CONFIG_SECTOR_START_ADDRESS,sizeof(uint32_t));
		
		uint32_t calcCrc = CalculateCRC32(config,sizeof(AirDeviceConfigTypeDef));
		if((calcCrc == storedCrc) && !IsDataErased((uint8_t*)config,sizeof(AirDeviceConfigTypeDef)))
		{
			return 0;
		}
	}
	
	GetDefaultAirDeviceConfig(config);
	
	for(uint8_t attCnt=0; attCnt < EEPROM_OP_ATTEMPTS_NUM; attCnt++)
	{			
			
		if(SaveAirDeviceConfigToEEPROM(config) == 0)
		{
			return 0;
		}
	}

	return 1;
}


uint8_t SaveAirDeviceConfigToEEPROM(AirDeviceConfigTypeDef *config)
{
	for(uint8_t cnt = 0; cnt < EEPROM_OP_ATTEMPTS_NUM; cnt++)
	{
		uint32_t crc = CalculateCRC32(config,sizeof(AirDeviceConfigTypeDef));	
  
		At25Wr((uint8_t*)&crc,CONFIG_SECTOR_START_ADDRESS,sizeof(uint32_t));
	
		At25Wr((uint8_t*)config,CONFIG_SECTOR_START_ADDRESS + sizeof(uint32_t),sizeof(AirDeviceConfigTypeDef));
	
		if(At25CompareWriteData(CONFIG_SECTOR_START_ADDRESS,(uint8_t*)&crc,sizeof(uint32_t)) != 0)
		{
			continue;
		}
		if(At25CompareWriteData(CONFIG_SECTOR_START_ADDRESS + sizeof(uint32_t),(uint8_t*)config,sizeof(AirDeviceConfigTypeDef)) != 0)
		{
			continue;
		}
		return 0;
	}
	return 1;
}

static void GetDefaultAirDeviceConfig(AirDeviceConfigTypeDef *config)
{
	memset(config,0,sizeof(AirDeviceConfigTypeDef));
	config->DeviceNameLen = strlen(DEFAULT_DEVICE_NAME);
	memcpy(config->DeviceName,DEFAULT_DEVICE_NAME,config->DeviceNameLen);
	config->WakeUpTimeSec = 30;
	config->IsDumperCalibrated = false;
	config->IsJournalEnabled = false;
	uint32_t err_code = nrf_drv_rng_init(NULL);
	APP_ERROR_CHECK(err_code);
	uint8_t rand[ADV_ID_LEN];
	err_code = random_vector_generate(rand,ADV_ID_LEN);
	APP_ERROR_CHECK(err_code);
	nrf_drv_rng_uninit();
	memcpy(config->AdvId, rand, ADV_ID_LEN);
}

static uint32_t random_vector_generate(uint8_t * p_buff, uint8_t size)
{
    uint32_t err_code;
    uint8_t  bytes_available = 0;

    nrf_drv_rng_bytes_available(&bytes_available);
    uint8_t retries = 0;
    
    while (bytes_available < size)
    {
        retries++;
        NRF_LOG_WARNING("Too few random bytes available. Trying again \r\n");
        nrf_drv_rng_bytes_available(&bytes_available);
        nrf_delay_ms(5);
        
        if (retries > 5)    // Return after n attempts.
        {
            return NRF_ERROR_TIMEOUT;
        }
    }
    
    NRF_LOG_INFO("Available random bytes: %d \r\n", bytes_available);

    err_code = nrf_drv_rng_rand(p_buff, size);
    APP_ERROR_CHECK(err_code);
    
    NRF_LOG_INFO("Random value (hex): ");
    
    for (uint8_t i = 0; i < size; i++)
    {
        NRF_LOG_RAW_INFO("%02x", p_buff[i]);
    }
    
    NRF_LOG_RAW_INFO("\r\n");

    return NRF_SUCCESS;
}


uint8_t AllignedReciveData[0xFF] __attribute__((aligned (sizeof(uint32_t))));

static void HandleEepromWriteData(uint8_t* data, uint16_t len)
{
	

  memcpy(AllignedReciveData,data,len);
	
	uint32_t fullAllignLen = (len / sizeof(uint32_t)) * sizeof(uint32_t);
	uint32_t stayLen = len % sizeof(uint32_t);
	ret_code_t ret_code;
	if(fullAllignLen > 0)
	{
			ret_code = nrf_fstorage_write(
        &fstorage,
        FW_MEMORY_ADDRESS + FileOffset,
        AllignedReciveData,
        fullAllignLen,
        NULL);
		APP_ERROR_CHECK(ret_code);
		while(!isWrited){}			
		isWrited = false;
		if(WriteResult != NRF_SUCCESS)
		{
			uint8_t txBleBuf[2];
			txBleBuf[0] = (uint8_t)CMD_RESP_MARKER_FILE_OP;
			txBleBuf[1] = (uint8_t)CMD_RESP_ERR_FILE_WRITE_ERROR;
			SendBleData(txBleBuf,2);
			
			return;
		}
		isWrited = false;
		
		FileOffset += fullAllignLen;
	}

	if(stayLen != 0)
	{
		uint8_t addWr[sizeof(uint32_t)] = {0xFF,0xFF,0xFF,0xFF,};
		
		for(uint8_t cnt=0;cnt < stayLen;cnt++)
		{
			addWr[cnt] = data[cnt];
		}
		
		ret_code = nrf_fstorage_write(
        &fstorage,
        FW_MEMORY_ADDRESS + FileOffset,
        addWr,
        sizeof(uint32_t),
        NULL);
		APP_ERROR_CHECK(ret_code);
		while(!isWrited){}			
		isWrited = false;
		if(WriteResult != NRF_SUCCESS)
		{
			uint8_t txBleBuf[2];
			txBleBuf[0] = (uint8_t)CMD_RESP_MARKER_FILE_OP;
			txBleBuf[1] = (uint8_t)CMD_RESP_ERR_FILE_WRITE_ERROR;
			SendBleData(txBleBuf,2);
			
			return;
		}
		isWrited = false;
		FileOffset += stayLen;
	}	


	uint8_t txBleBuf[2];
	txBleBuf[0] = (uint8_t)CMD_RESP_MARKER_FILE_OP;
	txBleBuf[1] = (uint8_t)CMD_RESP_OK;
	SendBleData(txBleBuf,2);
}



void HandleWriteCommand(uint8_t * data, uint32_t Len)
{
	HandleEepromWriteData(data, Len);
}



void HandleEndWriteCommand(void)
{	
		AirDeviceResponseEnum result = (AirDeviceResponseEnum)CheckEepromFirmwareIntegrity();
		
		uint8_t txBleBuf[2];
		txBleBuf[0] = (uint8_t)CMD_RESP_MARKER_FILE_OP;
		txBleBuf[1] = (uint8_t)result;
		SendBleData(txBleBuf,2);
		vTaskDelay(1000);
		FirmwareInfoTypeDef eepromfwInfo;
		FirmwareInfoTypeDef appfwInfo;
	
		GetEepromFirmwareInfo(&eepromfwInfo);
		GetApplicationFirmwareInfo(&appfwInfo);
		if(eepromfwInfo.Version > appfwInfo.Version && eepromfwInfo.Marker1 != 0xFFFFFFFF && eepromfwInfo.Marker2 != 0xFFFFFFFF &&
			eepromfwInfo.Marker3 != 0xFFFFFFFF)
		{
			NVIC_SystemReset();
		}
		
}

void PrepareEepromWrite(uint8_t * data, uint32_t Len)
{
	uint32_t fileAllocSize = *((uint32_t*)data);

	if(fileAllocSize > FW_MEMORY_SIZE)
	{
		uint8_t txBleBuf[2];
		txBleBuf[0] = (uint8_t)CMD_RESP_MARKER_FILE_OP;
		txBleBuf[1] = (uint8_t)CMD_RESP_ERR_FILE_WRONG_LEN;
		SendBleData(txBleBuf,2);
		return;
	}
	
	uint16_t pagesToEraseNum = fileAllocSize/NRF52_FLASH_PAGE_SIZE;
	if(fileAllocSize % NRF52_FLASH_PAGE_SIZE > 0)
	{
		pagesToEraseNum++;
	}
	
	if(pagesToEraseNum > FW_MEMORY_MAX_PAGES_NUM)
	{
		uint8_t txBleBuf[2];
		txBleBuf[0] = (uint8_t)CMD_RESP_MARKER_FILE_OP;
		txBleBuf[1] = (uint8_t)CMD_RESP_ERR_FILE_WRONG_LEN;
		SendBleData(txBleBuf,2);
		return;
	}
	//nrf_ble_scan_stop();
	isErased = false;
	ret_code_t ret_code = nrf_fstorage_erase(&fstorage, FW_MEMORY_ADDRESS, pagesToEraseNum, NULL);
  APP_ERROR_CHECK(ret_code);
	while(!isErased){}
	isErased = false;
	if(EraseResult != NRF_SUCCESS)
	{
		uint8_t txBleBuf[2];
		txBleBuf[0] = (uint8_t)CMD_RESP_MARKER_FILE_OP;
		txBleBuf[1] = (uint8_t)CMD_RESP_ERR_FILE_ERASE_ERROR;
		SendBleData(txBleBuf,2);
		return;
	}
	OnStartFirmawareUpdate();
	
	uint8_t txBleBuf[2];
	txBleBuf[0] = (uint8_t)CMD_RESP_MARKER_FILE_OP;
	txBleBuf[1] = (uint8_t)CMD_RESP_OK;
	SendBleData(txBleBuf,2);
	FileOffset = 0;	
}


void fstorage_evt_handler(nrf_fstorage_evt_t *p_evt)
{

	switch(p_evt->id)
	{
		case NRF_FSTORAGE_EVT_ERASE_RESULT:
		{
			isErased = true;
			EraseResult = p_evt->result;
			break;
		}
		case NRF_FSTORAGE_EVT_WRITE_RESULT:
		{
			
			isWrited = true;
			WriteResult = p_evt->result;
			break;
		}
		case NRF_FSTORAGE_EVT_READ_RESULT:
		{
			break;
		}
		
	}
}

