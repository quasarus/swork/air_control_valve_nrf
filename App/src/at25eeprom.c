#include "at25eeprom.h"
#include "nrf_delay.h"
#include "nrf_gpio.h"
#include "nrf_drv_ppi.h"
#include "airdevice_init.h"

/*AT25256B ���*/

//#define _W25QXX_USE_FREERTOS          1
//#define _W25QXX_DEBUG                 0

#define SPI_BUFFERS_LEN AT25_PAGE_SIZE

static uint8_t SpiRxBuf[SPI_BUFFERS_LEN] = {0};
static uint8_t SpiTxBuf[SPI_BUFFERS_LEN] = {0};

static void At25_Spi_Tx_Data(uint8_t	*txData,uint32_t len);
static void At25_Spi_Rx_Data(uint8_t	*rxData,uint32_t len);
static  void At25WrPage(uint8_t *pBuffer,uint32_t address,uint32_t NumBytes);
static void At25RdPage(uint8_t *pBuffer,uint32_t address,uint32_t NumBytes);
#if (_W25QXX_DEBUG==1)
#include <stdio.h>
#endif

#define SPI_DUMMY_BYTE         0xA5

//w25qxx_t	w25qxx;
//nrf_delay_ms(delay)



static volatile bool spis_xfer_done = false;
void At25_Spi_Tx(uint8_t	txData);
static uint8_t	At25_Spi_Rx(void);
static void spi_send_recv(uint8_t * const p_tx_data,
                          uint8_t * const p_rx_data,
                          const uint16_t  len);
static void At25_WriteEnable(void);
static void At25_WaitForWriteEnd(void);		
													
void SpiEepromEventHandle(nrf_drv_spi_evt_t const * p_event,void *p_context)
{
		uint32_t err_code;
		if (p_event->type == NRF_DRV_SPI_EVENT_DONE)
    {
			// ### Workaround Section Start ###
        
      // Disable the PPI channel
      err_code = nrf_drv_ppi_channel_disable(ppi_channel);
      APP_ERROR_CHECK(err_code);

      // Disable a GPIOTE output pin task.
      nrf_drv_gpiote_in_event_disable(EEPROM_SCK_PIN);
			spis_xfer_done = true;      
    }
}


static void spi_send_recv(uint8_t * const p_tx_data,
                          uint8_t * const p_rx_data,
                          const uint16_t  len)
{
    
    uint32_t err_code;

    if (len == 1){
            ret_code_t err_code;

            // Enable the PPI channel.
            err_code = nrf_drv_ppi_channel_enable(ppi_channel);
            APP_ERROR_CHECK(err_code);

            // Enable the GPIOTE output pin task.
            nrf_drv_gpiote_in_event_enable(EEPROM_SCK_PIN, false);

            //Start transfer of data
            err_code = nrf_drv_spi_transfer(&spi_eeprom, p_tx_data, len, p_rx_data, len);
            APP_ERROR_CHECK(err_code);
            }
            else{
                // Start transfer.
                err_code = nrf_drv_spi_transfer(&spi_eeprom, p_tx_data, len, p_rx_data, len);
                APP_ERROR_CHECK(err_code);
            }
}

void At25EnterUltraDeepSleep(void)
{
	nrf_gpio_pin_write(EEPROM_CS_PIN,0);
	At25_Spi_Tx(0x79);
	nrf_gpio_pin_write(EEPROM_CS_PIN,1);
}



static void At25_Spi_Tx(uint8_t	txData)
{	
	SpiTxBuf[0] = txData;
	spi_send_recv(SpiTxBuf,SpiRxBuf,1);
	while (!spis_xfer_done)
  {
    __WFE();
  }
	spis_xfer_done = false;
}

static uint8_t	At25_Spi_Rx()
{	
	SpiTxBuf[0] = SPI_DUMMY_BYTE;
  spi_send_recv(SpiTxBuf,SpiRxBuf,1);
	while (!spis_xfer_done)
  {
    __WFE();
  }
	spis_xfer_done = false;  	
	return SpiRxBuf[0];	
}

static void At25_Spi_Tx_Data(uint8_t	*txData,uint32_t len)
{		
	spi_send_recv(txData,SpiRxBuf,len);
	while (!spis_xfer_done)
  {
    __WFE();
  }
	spis_xfer_done = false;
}

static void At25_Spi_Rx_Data(uint8_t	*rxData,uint32_t len)
{
	memset(SpiTxBuf,SPI_DUMMY_BYTE,len);	
	SpiTxBuf[0] = SPI_DUMMY_BYTE;
  spi_send_recv(SpiTxBuf,rxData,len);
	while (!spis_xfer_done)
  {
    __WFE();
  }
	spis_xfer_done = false; 		
}

static void At25_WriteEnable(void)
{
	nrf_gpio_pin_write(EEPROM_CS_PIN,0);
  
  At25_Spi_Tx(AT25_WREN_EEPROM_CMD);
	nrf_gpio_pin_write(EEPROM_CS_PIN,1);
  
	nrf_delay_ms(1);
}

void At25_WriteDisable(void)
{
  
	nrf_gpio_pin_write(EEPROM_CS_PIN,0);
  At25_Spi_Tx(AT25_WRDI_EEPROM_CMD);
  
	nrf_gpio_pin_write(EEPROM_CS_PIN,1);
	nrf_delay_ms(1);
}

static void At25_WaitForWriteEnd(void)
{
	nrf_delay_ms(1);
	nrf_gpio_pin_write(EEPROM_CS_PIN,0);
	uint8_t statusReg;
	At25_Spi_Tx(AT25_RDSR_EEPROM_CMD);
  do
  {
    statusReg = At25_Spi_Rx();
		nrf_delay_ms(1);
  }
  while ((statusReg & 0x01) == 0x01);
	nrf_gpio_pin_write(EEPROM_CS_PIN,1); 
}

void At25Rd(uint8_t *pBuffer,uint32_t address,uint32_t NumBytes)
{
	uint32_t startPageAddress;
	uint16_t bytesToPageEnd;
	uint32_t bytesReaded = 0;
	while(bytesReaded < NumBytes)
	{
		startPageAddress = address + bytesReaded;
		bytesToPageEnd = AT25_PAGE_SIZE - (startPageAddress % AT25_PAGE_SIZE);
		
		uint32_t chunk;
		
		if(bytesToPageEnd != 0)
		{
			chunk = NumBytes - bytesReaded > bytesToPageEnd ? bytesToPageEnd : NumBytes - bytesReaded;
		}
		else
		{
			chunk = NumBytes - bytesReaded < AT25_PAGE_SIZE ? NumBytes - bytesReaded : AT25_PAGE_SIZE;
		}
		At25RdPage(pBuffer + bytesReaded,startPageAddress,chunk);
		
		bytesReaded+=chunk;
		 
	}		
	
}

void At25Wr(uint8_t *pBuffer,uint32_t address,uint32_t NumBytes)
{
	uint32_t startPageAddress;
	uint16_t bytesToPageEnd;
	uint32_t bytesWrited = 0;
	while(bytesWrited < NumBytes)
	{
		startPageAddress = address + bytesWrited;
		bytesToPageEnd = AT25_PAGE_SIZE - (startPageAddress % AT25_PAGE_SIZE);
		
		uint32_t chunk;
		
		if(bytesToPageEnd != 0)
		{
			chunk = NumBytes - bytesWrited > bytesToPageEnd ? bytesToPageEnd : NumBytes - bytesWrited;
		}
		else
		{
			chunk = NumBytes - bytesWrited < AT25_PAGE_SIZE ? NumBytes - bytesWrited : AT25_PAGE_SIZE;
		}
		At25WrPage(pBuffer + bytesWrited,startPageAddress,chunk);
		
		bytesWrited+=chunk;
		 
	}		
	
}



static void At25RdPage(uint8_t *pBuffer,uint32_t address,uint32_t NumBytes)
{	
	uint32_t bytesReaded = 0;
	nrf_gpio_pin_write(EEPROM_CS_PIN,0);
	At25_Spi_Tx(AT25_READ_EEPROM_CMD);
	At25_Spi_Tx((address& 0x7F00) >> 8);
	At25_Spi_Tx((uint8_t)address);
	while(bytesReaded < NumBytes)
	{
		uint32_t chunk = (NumBytes - bytesReaded < AT25_PAGE_SIZE) ? NumBytes - bytesReaded : AT25_PAGE_SIZE;
		At25_Spi_Rx_Data(pBuffer + bytesReaded,chunk);
		bytesReaded += chunk;
	}		
	nrf_gpio_pin_write(EEPROM_CS_PIN,1);
}

static  void At25WrPage(uint8_t *pBuffer,uint32_t address,uint32_t NumBytes)
{	
	At25_WaitForWriteEnd();
  At25_WriteEnable();
	uint32_t bytesWrited = 0;
	nrf_gpio_pin_write(EEPROM_CS_PIN,0);
	At25_Spi_Tx(AT25_WRITE_EEPROM_CMD);
	At25_Spi_Tx((address& 0x7F00) >> 8);
	At25_Spi_Tx((uint8_t)address);
  
	while(bytesWrited < NumBytes)
	{
		uint32_t chunk = (NumBytes - bytesWrited < AT25_PAGE_SIZE) ? NumBytes - bytesWrited : AT25_PAGE_SIZE;
		At25_Spi_Tx_Data(pBuffer + bytesWrited,chunk);
		bytesWrited += chunk;
		nrf_delay_ms(1);		
	}	
	nrf_gpio_pin_write(EEPROM_CS_PIN,1);	
	At25_WaitForWriteEnd();	
	nrf_delay_ms(1);
	At25_WriteDisable();
}

uint8_t At25CompareWriteData(uint32_t eeprom_Address,uint8_t *pBuffer,uint32_t len)
{
	uint32_t bytesReaded = 0;
	nrf_gpio_pin_write(EEPROM_CS_PIN,0);
	At25_Spi_Tx(AT25_READ_EEPROM_CMD);
	At25_Spi_Tx((eeprom_Address& 0x7F00) >> 8);
	At25_Spi_Tx((uint8_t)eeprom_Address);
	while(bytesReaded < len)
	{
		uint8_t rdByte = At25_Spi_Rx();
		if(*(pBuffer + bytesReaded) != rdByte)
		{
			nrf_gpio_pin_write(EEPROM_CS_PIN,1);
			return 1;
		}
		
		bytesReaded++;
	}		
	nrf_gpio_pin_write(EEPROM_CS_PIN,1);
	return 0;
}
