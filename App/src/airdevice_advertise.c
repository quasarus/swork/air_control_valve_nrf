#include "airdevice_advertise.h"

BLE_ADVERTISING_DEF(m_advertising);                                                 /**< Advertising module instance. */

//static ble_uuid_t m_adv_uuids[]          =                                          /**< Universally unique service identifier. */
//{
//    {AIRDEV_SERVICE_DATA_ID_1, BLE_UUID_TYPE_BLE},
//		{AIRDEV_SERVICE_DATA_ID_2, BLE_UUID_TYPE_BLE},
////		{0x0000, BLE_UUID_TYPE_BLE},
////		{0x0102, BLE_UUID_TYPE_BLE},
////		{0x0304, BLE_UUID_TYPE_BLE},
////		{0x0506, BLE_UUID_TYPE_BLE},
////		{0x0708, BLE_UUID_TYPE_BLE},
////		{0x0910, BLE_UUID_TYPE_BLE},
////		{0x1112, BLE_UUID_TYPE_BLE},
////		{0x1314, BLE_UUID_TYPE_BLE},
////		{0x1516, BLE_UUID_TYPE_BLE},
//};

static bool isBaseDeviceVisible = false;

static void on_adv_evt(ble_adv_evt_t ble_adv_evt);

static void GetAdvManufData(ble_advdata_manuf_data_t *mData);
static void GetSrespManufData(ble_advdata_manuf_data_t *mData, AirDeviceConfigTypeDef *config);


static ble_advdata_manuf_data_t mDataForAdv;
static ble_advdata_manuf_data_t mDataForSresp;


void AdvertisingInit(AirDeviceConfigTypeDef *config)
{
	 uint32_t               err_code;
   ble_advertising_init_t init;
		//ble_uuid128_t   service128bit_uuid = AIRDEVICE_BASE_UUID_ADV;
   memset(&init, 0, sizeof(init));

		//ble_advdata_service_data_t service_data_struct;
	
		
   init.srdata.name_type          = BLE_ADVDATA_NO_NAME;
		
   init.advdata.include_appearance = false;
   init.advdata.flags              = BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE;
	
			
		
   init.config.ble_adv_fast_enabled  = true;
   init.config.ble_adv_fast_interval = APP_ADV_INTERVAL;
   init.config.ble_adv_fast_timeout  = APP_ADV_DURATION;	
		
   init.evt_handler = on_adv_evt;

		GetAdvManufData(&mDataForAdv);
		GetSrespManufData(&mDataForSresp,config);		

		init.advdata.p_manuf_specific_data = &mDataForAdv;
    init.srdata.p_manuf_specific_data = &mDataForSresp;

		
    err_code = ble_advertising_init(&m_advertising, &init);
    NRF_LOG_INFO("Err %d.", err_code);
		APP_ERROR_CHECK(err_code);
		
		//m_advertising.adv_data.adv_data.p_data[2] = 0x06;
		
		
    ble_advertising_conn_cfg_tag_set(&m_advertising, APP_BLE_CONN_CFG_TAG);
}

void AdvertiseStart(void)
{
	uint32_t err_code = ble_advertising_start(&m_advertising, BLE_ADV_MODE_FAST);		
  APP_ERROR_CHECK(err_code);
}

void UpdateAdvData(void)
{
		uint32_t               err_code;
    ble_advertising_init_t init;
		
    memset(&init, 0, sizeof(init));		
		
    init.srdata.name_type          = BLE_ADVDATA_NO_NAME;
		
    init.advdata.include_appearance = false;
    init.advdata.flags              = BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE;
	
			
		
    init.config.ble_adv_fast_enabled  = true;
    init.config.ble_adv_fast_interval = APP_ADV_INTERVAL;
    init.config.ble_adv_fast_timeout  = APP_ADV_DURATION;	
		

		
		GetAdvManufData(&mDataForAdv);
		
		init.advdata.p_manuf_specific_data = &mDataForAdv;
    init.srdata.p_manuf_specific_data = &mDataForSresp;

		err_code = ble_advertising_advdata_update(&m_advertising,&init.advdata,&init.srdata);
		APP_ERROR_CHECK(err_code);
}

void UpdateRespData(AirDeviceConfigTypeDef *config)
{
	uint32_t               err_code;
  ble_advertising_init_t init;
		
  memset(&init, 0, sizeof(init));		
		
  init.srdata.name_type          = BLE_ADVDATA_NO_NAME;
		
  init.advdata.include_appearance = false;
  init.advdata.flags              = BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE;
	
			
		
  init.config.ble_adv_fast_enabled  = true;
  init.config.ble_adv_fast_interval = APP_ADV_INTERVAL;
  init.config.ble_adv_fast_timeout  = APP_ADV_DURATION;	
		
	GetSrespManufData(&mDataForSresp,config);
		
	init.advdata.p_manuf_specific_data = &mDataForAdv;
  init.srdata.p_manuf_specific_data = &mDataForSresp;

	err_code = ble_advertising_advdata_update(&m_advertising,&init.advdata,&init.srdata);
	APP_ERROR_CHECK(err_code);
}

static void on_adv_evt(ble_adv_evt_t ble_adv_evt)
{
    uint32_t err_code;

    switch (ble_adv_evt)
    {
        case BLE_ADV_EVT_FAST:
            //err_code = bsp_indication_set(BSP_INDICATE_ADVERTISING);
            //APP_ERROR_CHECK(err_code);
            break;
        case BLE_ADV_EVT_IDLE:
            //sleep_mode_enter();
            break;
        default:
            break;
    }
}

static uint8_t DataAdv[] = {BLE_ADV_ID_BYTE1,BLE_ADV_ID_BYTE2,BLE_ADV_ID_BYTE_VALVE_DEVICE,0x00,0x00};

static void GetAdvManufData(ble_advdata_manuf_data_t *mData)
{	
	if(isBaseDeviceVisible)
	{
		DataAdv[3] |= ADV_DAT_FLAG_BASE_DEV_VISIBLE;
	}
	else
	{
		DataAdv[3] &= ~ADV_DAT_FLAG_BASE_DEV_VISIBLE;
	}
	
	if(IsBatteryLow())
	{
		DataAdv[3] |= ADV_DAT_FLAG_LOW_BATTERY;
	}
	else
	{
		DataAdv[3] &= ~ADV_DAT_FLAG_LOW_BATTERY;
	}
		
	
	mData->company_identifier = NO_COMPANY_MANUF_DATA_ID;
	mData->data.size=sizeof(DataAdv);
	mData->data.p_data = DataAdv;
}

static uint8_t DataResp[DEVICE_NAME_MAX_LEN] = {0};

static void GetSrespManufData(ble_advdata_manuf_data_t *mData, AirDeviceConfigTypeDef *config)
{
	mData->company_identifier = NO_COMPANY_MANUF_DATA_ID;
	
	uint8_t cpySize = config->DeviceNameLen > DEVICE_NAME_MAX_LEN ? DEVICE_NAME_MAX_LEN : config->DeviceNameLen;
	memset(DataResp,0,sizeof(DataResp));
	memcpy(DataResp,config->DeviceName,cpySize);
	
	mData->data.size = (cpySize > sizeof(DataAdv) + 2) ? cpySize : (sizeof(DataAdv) + 3);
	mData->data.p_data = DataResp;
}

void SetBaseDeviceVisibility(bool isVisible)
{
	isBaseDeviceVisible = isVisible;
}


void HandleSetDeviceNameCommand(uint8_t * data, uint32_t len, AirDeviceConfigTypeDef *config)
{
	if(len > DEVICE_NAME_MAX_LEN)
	{
		uint8_t txBleBuf[2];
		txBleBuf[0] = (uint8_t)CMD_RESP_MARKER_DEVICE_NAME_SET;
		txBleBuf[1] = (uint8_t)CMD_RESP_ERR_DEV_NAME_WRONG_SIZE;
		SendBleData(txBleBuf,2);
		return;
	}
	
	config->DeviceNameLen = len;
	memcpy(config->DeviceName,data,len);
	
	AirDeviceResponseEnum resp = SaveAirDeviceConfigToEEPROM(config) == 0 ?
	CMD_RESP_OK : CMD_RESP_ERR_DEV_NAME_SAVE_ERROR;
	
	if(resp == CMD_RESP_OK)
	{
		UpdateRespData(config);
	}	
	
	uint8_t txBleBuf[2];
	txBleBuf[0] = (uint8_t)CMD_RESP_MARKER_DEVICE_NAME_SET;
	txBleBuf[1] = (uint8_t)resp;
	SendBleData(txBleBuf,2);
	
}

