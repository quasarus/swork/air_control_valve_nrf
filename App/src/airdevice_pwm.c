#include "airdevice_pwm.h"

void StartMotor(bool isCw)
{	

	
	if(isCw)
	{
	  static nrf_pwm_values_individual_t /*const*/ seq_values[] =
    {
				{ 0,          0x8000,     0x8000,     0x8000      },							//1 ������ �������				
        { 0x8000,     0,          0x8000,     0x8000      },							//2 ������ �������				
        { 0x8000,     0x8000,     0,          0x8000      },				//3 ������ �������				
        { 0x8000,     0x8000,     0x8000, 	  0           }					//4 ������ ������� 	
    };
		    nrf_pwm_sequence_t const seq =
    {
        .values.p_individual = seq_values,
        .length              = NRF_PWM_VALUES_LENGTH(seq_values),
        .repeats             = 0,
        .end_delay           = 0
    };
    (void)nrf_drv_pwm_simple_playback(&m_pwm0, &seq, 1, NRF_DRV_PWM_FLAG_LOOP);
	}
	else
	{
		//TODO �������� ��� �������� ������ �������

	  static nrf_pwm_values_individual_t /*const*/ seq_values[] =
    {
				{ 0x8000,     0x8000,     0x8000,     0      },							//1 ������ �������				
        { 0x8000,     0x8000,     0,          0x8000      },							//2 ������ �������				
        { 0x8000,     0,          0x8000,     0x8000      },				//3 ������ �������				
        { 0,          0x8000,     0x8000, 	  0x8000           }					//4 ������ ������� 	       
    };		
		
    nrf_pwm_sequence_t const seq =
    {
        .values.p_individual = seq_values,
        .length              = NRF_PWM_VALUES_LENGTH(seq_values),
        .repeats             = 0,
        .end_delay           = 0
    };
    (void)nrf_drv_pwm_simple_playback(&m_pwm0, &seq, 1, NRF_DRV_PWM_FLAG_LOOP);		
	}	
}

void StopMotor(void)
{
	nrfx_pwm_stop(&m_pwm0,false);
	Control5VoltPower(false);
	Control5VoltDcDc(false);
}

