#include "airdevice_init.h"

nrf_drv_pwm_t m_pwm0 = NRF_DRV_PWM_INSTANCE(0);
const nrf_drv_spi_t spi_eeprom = NRF_DRV_SPI_INSTANCE(EEPROM_SPI_INSTANCE_ID);




const nrf_drv_twi_t hum_enc_twi = NRF_DRV_TWI_INSTANCE(ENCODER_HUM_TWI_INSTANCE_ID);
const nrf_drv_timer_t timeout_timer = NRF_DRV_TIMER_INSTANCE(3);

//const nrf_drv_rtc_t wake_up_rtc = NRF_DRV_RTC_INSTANCE(2);
const nrf_drv_rtc_t calendar_rtc = NRF_DRV_RTC_INSTANCE(2);
static void TimeoutTimerInit(void);

NRF_FSTORAGE_DEF(nrf_fstorage_t fstorage) =
	{
		.evt_handler = fstorage_evt_handler,
		.start_addr  = (uint32_t)FW_MEMORY_ADDRESS,
		.end_addr    = (uint32_t)FW_MEMORY_ADDRESS + FW_MEMORY_SIZE};

void saadc_sampling_event_init(void);
void saadc_sampling_event_enable(void);

nrf_ppi_channel_t ppi_channel;




//Dummy handler
void in_pin_handler( nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action)
{
    //Do nothing
}


void SpiInit(void)
{
	
	nrf_drv_spi_config_t spi_eeprom_config = NRF_DRV_SPI_DEFAULT_CONFIG;
	
  spi_eeprom_config.ss_pin   = NRFX_SPI_PIN_NOT_USED;  
  spi_eeprom_config.mosi_pin = EEPROM_MOSI_PIN;
	spi_eeprom_config.miso_pin  = EEPROM_MISO_PIN;
  spi_eeprom_config.sck_pin  = EEPROM_SCK_PIN;
	spi_eeprom_config.frequency = NRF_DRV_SPI_FREQ_4M;
	
	APP_ERROR_CHECK(nrf_drv_spi_init(&spi_eeprom, &spi_eeprom_config, SpiEepromEventHandle,NULL));	
	
	// ### Workaround Section Start ###
    
     uint32_t err_code;
    
     NRF_SPIM_Type * p_spim = spi_eeprom.u.spim.p_reg;
    
    //Initialize GPIOTE and PPI drivers for the workaround
    
    err_code = nrf_drv_gpiote_init();
    APP_ERROR_CHECK(err_code);
    
    // Initializes the GPIOTE channel so that SCK toggles generates events
    nrf_drv_gpiote_in_config_t config = GPIOTE_CONFIG_IN_SENSE_TOGGLE(true);
            err_code = nrf_drv_gpiote_in_init(EEPROM_SCK_PIN, &config, in_pin_handler);
            APP_ERROR_CHECK(err_code); 
        
    err_code = nrf_drv_ppi_init();
    APP_ERROR_CHECK(err_code);
    
    // Allocate first unused PPI channel 
    err_code = nrf_drv_ppi_channel_alloc(&ppi_channel);
    APP_ERROR_CHECK(err_code);
    
    // Assign the PPI channel to the SCK pin
    err_code = nrf_drv_ppi_channel_assign(ppi_channel,
                                        nrf_drv_gpiote_in_event_addr_get(EEPROM_SCK_PIN),
                                        (uint32_t)&p_spim->TASKS_STOP);
    APP_ERROR_CHECK(err_code);
	
}


void GpioInit(void)
{
	//nrf_gpio_cfg_output(BUTTON_PIN);		//TODO  ��� ����

	//nrf_gpio_cfg_output(LED_IND_PIN);

//	//nrf_gpio_cfg_output(ENC_POWER_EN_PIN);

	//nrf_gpio_cfg_output(VCC_T_EN_PIN);
	nrf_gpio_cfg_output(EN_5V_PIN);

	nrf_gpio_cfg_output(DC5V_EN_PIN);
//	nrf_gpio_cfg_output(ADC_EN_PIN);

	ControlLed(false);

	
	Control5VoltDcDc(false);
	Control5VoltPower(false);
	ControlTemperatureSensor(false);
	ControlEncoderPower(false);
	ControlBatteryAdcDivider(false);


	nrf_gpio_cfg_output(EEPROM_CS_PIN);	
	nrf_gpio_pin_write(EEPROM_CS_PIN,1);


}

void TimersInit(void)
{
	ret_code_t err_code = app_timer_init();
  APP_ERROR_CHECK(err_code);
	TimeoutTimerInit();
	
	CalendarTimerInit();
	//WakeUpTimerInit();
}

void AdcInit(void)
{
	//ENCODER_SAADC_INPUT
	//ControlBatteryAdcDivider(true);
	ret_code_t err_code;
  nrf_saadc_channel_config_t channel_config =
  NRF_DRV_SAADC_DEFAULT_CHANNEL_CONFIG_SE(BATTERY_MEAS_SAADC_INPUT);

	channel_config.gain = NRF_SAADC_GAIN1_4;
	
  err_code = nrf_drv_saadc_init(NULL, saadc_callback);
  APP_ERROR_CHECK(err_code);

  err_code = nrf_drv_saadc_channel_init(ADC_VCC_CHANNEL, &channel_config);
  APP_ERROR_CHECK(err_code);
	
	nrf_saadc_value_t val;
	err_code = nrf_drv_saadc_sample_convert(ADC_VCC_CHANNEL, &val);	
  APP_ERROR_CHECK(err_code);
	//ControlBatteryAdcDivider(false);
	
	nrf_saadc_channel_config_t channel_enc_config = NRF_DRV_SAADC_DEFAULT_CHANNEL_CONFIG_SE(ENCODER_SAADC_INPUT);
	channel_enc_config.gain = NRF_SAADC_GAIN1_6;
	
	err_code = nrf_drv_saadc_channel_init(ADC_ENCODER_CHANNEL, &channel_enc_config);
  APP_ERROR_CHECK(err_code);
	
//	err_code = nrf_drv_saadc_sample_convert(ADC_ENCODER_CHANNEL, &val);	
//  APP_ERROR_CHECK(err_code);
}



void saadc_sampling_event_init(void)
{
//    ret_code_t err_code;

//    err_code = nrf_drv_ppi_init();
//    APP_ERROR_CHECK(err_code);

//    nrf_drv_timer_config_t timer_cfg = NRF_DRV_TIMER_DEFAULT_CONFIG;
//    timer_cfg.bit_width = NRF_TIMER_BIT_WIDTH_32;
//    err_code = nrf_drv_timer_init(&m_timer, &timer_cfg, timer_handler);
//    APP_ERROR_CHECK(err_code);

//    /* setup m_timer for compare event every 400ms */
//    uint32_t ticks = nrf_drv_timer_ms_to_ticks(&m_timer, 400);
//    nrf_drv_timer_extended_compare(&m_timer,
//                                   NRF_TIMER_CC_CHANNEL0,
//                                   ticks,
//                                   NRF_TIMER_SHORT_COMPARE0_CLEAR_MASK,
//                                   false);
//    nrf_drv_timer_enable(&m_timer);

//    uint32_t timer_compare_event_addr = nrf_drv_timer_compare_event_address_get(&m_timer,
//                                                                                NRF_TIMER_CC_CHANNEL0);
//    uint32_t saadc_sample_task_addr   = nrf_drv_saadc_sample_task_get();

//    /* setup ppi channel so that timer compare event is triggering sample task in SAADC */
//    err_code = nrf_drv_ppi_channel_alloc(&m_ppi_channel);
//    APP_ERROR_CHECK(err_code);

//    err_code = nrf_drv_ppi_channel_assign(m_ppi_channel,
//                                          timer_compare_event_addr,
//                                          saadc_sample_task_addr);
//    APP_ERROR_CHECK(err_code);
}


void saadc_sampling_event_enable(void)
{
//    ret_code_t err_code = nrf_drv_ppi_channel_enable(m_ppi_channel);

//    APP_ERROR_CHECK(err_code);
}

static uint8_t m_used = 0;
void PwmInit(void)
{
	  nrf_drv_pwm_config_t const config0 =
    {
        .output_pins =
        {
            MOTOR_PWM_PIN_A, // channel 0
            MOTOR_PWM_PIN_B, // channel 1
            MOTOR_PWM_PIN_C, // channel 2
            MOTOR_PWM_PIN_D, // channel 3
        },
        .irq_priority = APP_IRQ_PRIORITY_LOWEST,
        .base_clock   = PWM_PRESCALER_PRESCALER_DIV_16,
        .count_mode   = NRF_PWM_MODE_UP,
        .top_value    = MOTOR_TACT_DURATION_MS,
        .load_mode    = NRF_PWM_LOAD_INDIVIDUAL,
        .step_mode    = NRF_PWM_STEP_AUTO
    };
    APP_ERROR_CHECK(nrf_drv_pwm_init(&m_pwm0, &config0, NULL));
    m_used |= USED_PWM(0);
		
//		StartMotor(false);
//    nrf_delay_ms(5000);
//		StopMotor();
//		StartMotor(false);
//    nrf_delay_ms(5000);
//		StopMotor();			
}

void TwiInit(void)
{
	  ret_code_t err_code;
		
    const nrf_drv_twi_config_t twi_hum_config = {
       .scl                = HUM_SENSOR_SCL_PIN,
       .sda                = HUM_SENSOR_SDA_PIN,
       .frequency          = NRF_DRV_TWI_FREQ_100K,
       .interrupt_priority = APP_IRQ_PRIORITY_HIGH,
       //.clear_bus_init     = false
    };

    err_code = nrf_drv_twi_init(&hum_enc_twi, &twi_hum_config, enc_hum_handler, NULL);
    APP_ERROR_CHECK(err_code);
    nrf_drv_twi_enable(&hum_enc_twi);	
		
#ifdef ENCODER_EXISTS
		//EncoderConfig();
#endif
		
}

void FstorageInit(void)
{
	ret_code_t ret_code = nrf_fstorage_init(&fstorage, &nrf_fstorage_sd, NULL);
	APP_ERROR_CHECK(ret_code);


}



static void TimeoutTimerInit(void)
{
	 uint32_t time_ms = 5000; //Time(in miliseconds) between consecutive compare events.
   uint32_t time_ticks;
   uint32_t err_code = NRF_SUCCESS;
	
	 nrf_drv_timer_config_t timer_cfg = NRF_DRV_TIMER_DEFAULT_CONFIG;
	 timer_cfg.frequency = NRF_TIMER_FREQ_31250Hz;
	 timer_cfg.bit_width = TIMER_BITMODE_BITMODE_32Bit;
   err_code = nrf_drv_timer_init(&timeout_timer, &timer_cfg, TimeoutTimerEventHandler);
   APP_ERROR_CHECK(err_code);

}

//void WakeUpTimerInit(void)
//{
//	 /* uint32_t err_code;

//    //Initialize RTC instance
//    nrf_drv_rtc_config_t config = NRF_DRV_RTC_DEFAULT_CONFIG;
//    config.prescaler = 4095;
//    err_code = nrf_drv_rtc_init(&wake_up_rtc, &config, WakeUpTimerEventHandler);
//    APP_ERROR_CHECK(err_code);

//    //Enable tick event & interrupt
//    //nrf_drv_rtc_tick_enable(&rtc,true);

//    //Set compare channel to trigger interrupt after COMPARE_COUNTERTIME seconds
//    err_code = nrf_drv_rtc_cc_set(&wake_up_rtc,0,AirDeviceConfig.WakeUpTimeSec * 8,true);
//    APP_ERROR_CHECK(err_code);

//    //Power on RTC instance
//    nrf_drv_rtc_enable(&wake_up_rtc);
//   //nrf_drv_timer_enable(&wake_up_timer);*/
//	
//	
//}

void CalendarTimerInit(void)
{
	uint32_t err_code;
	nrf_drv_rtc_config_t config = NRF_DRV_RTC_DEFAULT_CONFIG;
	config.prescaler = 4095;				//PRESCALER = round(32.768 kHz / 8 Hz) � 1 = 4095
	
	err_code = nrf_drv_rtc_init(&calendar_rtc, &config, CalendatTimerEventHandler);
  APP_ERROR_CHECK(err_code);
	
	nrfx_rtc_overflow_enable(&calendar_rtc,true);
}

