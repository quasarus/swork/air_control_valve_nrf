#include "airdevice_dumper.h"


//static uint8_t TxBleData[2];
void HandleDumperPositionSet(BleTransactDataTypeDef *transactData,uint8_t pos)
{
	switch(pos)
	{
		case 1:
		{			
			AddDumperPosToQueue(DUMPER_POS_FULL_OPEN,JEVENT_TRIGGER_USER);
			break;
		}
		case 2:
		{
			AddDumperPosToQueue(DUMPER_POS_PARTLY_5_6,JEVENT_TRIGGER_USER);
			break;
		}
		case 3:
		{
			AddDumperPosToQueue(DUMPER_POS_PARTLY_4_6,JEVENT_TRIGGER_USER);
			break;
		}
		case 4:
		{
			AddDumperPosToQueue(DUMPER_POS_PARTLY_3_6,JEVENT_TRIGGER_USER);
			break;
		}
		case 5:
		{
			AddDumperPosToQueue(DUMPER_POS_PARTLY_2_6,JEVENT_TRIGGER_USER);
			break;
		}
		case 6:
		{
			AddDumperPosToQueue(DUMPER_POS_PARTLY_1_6,JEVENT_TRIGGER_USER);
			break;
		}
		case 7:
		{
			AddDumperPosToQueue(DUMPER_POS_FULL_CLOSE,JEVENT_TRIGGER_USER);
			break;
		}
		default:
			break;
	}	

}

void HandleGetFwVersionCommand(void)
{
	uint8_t txBleBuf[5];
	txBleBuf[0] = CMD_RESP_MARKER_FW_VERSION;	
	txBleBuf[1] = (uint8_t)FirmwareInfo.Version;
	txBleBuf[2] = (uint8_t)(FirmwareInfo.Version >> 8);
	txBleBuf[3] = (uint8_t)(FirmwareInfo.Version >> 16);
	txBleBuf[4] = (uint8_t)(FirmwareInfo.Version >> 24);	
	SendBleData(txBleBuf,5);
		
}


