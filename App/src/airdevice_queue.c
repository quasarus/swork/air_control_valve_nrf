#include "airdevice_queue.h"
#include "airdevice_alg1.h"

static uint8_t RxDataQueueBuf[QUEUE_RX_DATA_SIZE];
//static uint8_t TxDataQueueBuf[QUEUE_TX_DATA_SIZE];

//static uint32_t RxDataQueueBufOffset = 0;
//static uint32_t TxDataQueueBufOffset = 0;

QueueHandle_t DumperPosQueue;

SemaphoreHandle_t xSemaphoreHum;

#if defined (UART_PRESENT)
static TaskHandle_t  ble_dbg_task_handle;

static void dbg_task_function (void * pvParameter);
#endif

static TaskHandle_t dumper_task_handle;
//static TaskHandle_t  ble_tx_task_handle;
static TaskHandle_t ble_rx_task_handle;
static TaskHandle_t hum_task_handle;

static TaskHandle_t wake_up_task_handle;
//static TaskHandle_t idle_task_handle;
//static TaskHandle_t adc_task_handle;

static void ble_rx_task_function (void * pvParameter);
//static void ble_tx_task_function (void * pvParameter);
static void hum_task_function (void * pvParameter);
static void wake_up_task_function (void * pvParameter);

//static void main_task_function (void * pvParameter);
//static void idle_task_function (void * pvParameter);
static int RxQueueElementHandler(uint8_t * pItem, uint32_t itemLen);
static int TxElementHandler(uint8_t * pItem, uint32_t itemLen);

void OsInit(void)
{

//	if(xTaskCreate(ble_tx_task_function, "BLE1", configMINIMAL_STACK_SIZE + 300*4, (void*)&BleTransactData, 1, &ble_tx_task_handle) != pdPASS)
//	{
//		NRF_LOG_ERROR("Can not create task");
//		APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
//	}
	if(xTaskCreate(ble_rx_task_function, "BLE2", configMINIMAL_STACK_SIZE + 300*4, (void*)&BleTransactData, 1, &ble_rx_task_handle) != pdPASS)
	{
		NRF_LOG_ERROR("Can not create task");
		APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
	}
	if(xTaskCreate(hum_task_function, "hum", configMINIMAL_STACK_SIZE + 300*4, NULL, 1, &hum_task_handle) != pdPASS)
	{
		NRF_LOG_ERROR("Can not create task");
		APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
	}
	if(xTaskCreate(wake_up_task_function, "wup", configMINIMAL_STACK_SIZE + 300*4, (void*)&AirDeviceConfig, 1, &wake_up_task_handle) != pdPASS)
	{
		NRF_LOG_ERROR("Can not create task");
		APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
	}
	if(xTaskCreate(dumper_task_function, "dumper", configMINIMAL_STACK_SIZE + 300*4, (void*)&AirDeviceConfig, 1, &dumper_task_handle) != pdPASS)
	{
		NRF_LOG_ERROR("Can not create task");
		APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
	}
	
	xSemaphoreHum  = xSemaphoreCreateMutex();
	if(xSemaphoreHum == NULL)
	{
		NRF_LOG_ERROR("Can not create Semaphore");
		APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
	}
	BleTransactData.RxEmptyItemsQueue = xQueueCreate(QUEUE_RX_MAX_LEN, sizeof(BleQueueItemTypeDef));
	BleTransactData.RxFullItemsQueue = xQueueCreate(QUEUE_RX_MAX_LEN, sizeof(BleQueueItemTypeDef));
	ResetRxQueues();
	
	DumperPosQueue = xQueueCreate(DUMPER_POS_QUEUE_LEN, sizeof(DumperPositionItemTypeDef));
	
	if(BleTransactData.RxEmptyItemsQueue == NULL || BleTransactData.RxFullItemsQueue == NULL || DumperPosQueue == NULL)
	{
		NRF_LOG_ERROR("Can not create queue");
		APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
	}
}

void ResetRxQueues(void)
{
	xQueueReset(BleTransactData.RxEmptyItemsQueue);
	xQueueReset(BleTransactData.RxFullItemsQueue);
	for(uint8_t cnt = 0; cnt < QUEUE_RX_MAX_LEN; cnt++)
	{
		BleQueueItemTypeDef freeItem;
		freeItem.pItem = RxDataQueueBuf + cnt*QUEUE_DATA_ITEM_SIZE;
		freeItem.ItemSize = 0;
		xQueueSend(BleTransactData.RxEmptyItemsQueue,&freeItem,10);
	}
}

//���������� �������� �������� �������
static int RxQueueElementHandler(uint8_t * pItem, uint32_t itemLen)
{

	switch((AirDeviceCommandTypeDef)pItem[0])
	{
		case CMD_SET_DAMPER_POS:
		{
			if(itemLen != 2)
			{
				return -1;
			}
			HandleDumperPositionSet(&BleTransactData, pItem[1]);
			break;
		}
		case CMD_WRITE_TO_EEPROM:
		{				
			HandleWriteCommand(pItem + 1,itemLen - 1);
			break;
		}
		case CMD_END_WRITE_TO_EEPROM: 
		{		
			HandleEndWriteCommand();;
			break;
		}
		case CMD_PREPARE_TO_WRITE_TO_EEPROM:
		{
			PrepareEepromWrite(pItem + 1,itemLen - 1);
			break;
		}
		case CMD_GET_TEMPERATURE_AND_HUMIDITY:
		{
			HandleGetTemperstureAndHumidityCommand(pItem,itemLen);
			break;
		}
		case CMD_GET_FW_VERSION:
		{			
			HandleGetFwVersionCommand();
			break;
		}
		case CMD_SET_DEVICE_NAME:
		{			
			HandleSetDeviceNameCommand(pItem + 1,itemLen - 1,&AirDeviceConfig);
			break;
		}
		case CMD_SET_WAKEUP_INTERVAL_SET:
		{
			HandleSetWakeUpIntervalCommand(pItem + 1,itemLen - 1,&AirDeviceConfig);
			break;
		}
		case CMD_SET_WAKEUP_INTERVAL_GET:
		{
			HandleGetWakeUpIntervalCommand(&AirDeviceConfig);
			break;
		}
		case CMD_DUMPER_CALIBRATE:
		{
			HandleDumperCalibrationCommand(JEVENT_TRIGGER_USER);
			break;
		}
		case CMD_DATETIME_SET:
		{
			HandleSetDateTimeCommand(pItem + 1,itemLen - 1);
			break;
		}
		case CMD_DATETIME_GET:
		{
			HandleGetDateTimeCommand();
			break;
		}
		case CMD_SET_EN_DIS_JOURNAL:
		{			
			HandleSetEnDisJournalCommand(&AirDeviceConfig,pItem + 1,itemLen - 1);
			break;
		}
		case CMD_GET_EN_DIS_JOURNAL:
		{			
			HandleGetEnDisJournalCommand(&AirDeviceConfig);
			break;
		}
		case CMD_GET_JOURNAL_EVENTS:
		{			
			HandleGetJournalEventsCommand(pItem + 1,itemLen - 1);
			break;
		}
		case CMD_GET_JOURNAL_MEASURES:
		{			
			HandleGetJournalMeasuresCommand(pItem + 1,itemLen - 1);
			break;
		}
		default:
			return -1;
	}
	return 0;
}


//���������� �������� ���������� �������
static int TxElementHandler(uint8_t * pItem, uint32_t itemLen)
{						
		uint16_t length = (uint16_t)itemLen;
		//
		return ble_airdevice_data_send(BleTransactData.pAirDevInstance,pItem,&length,GetConnectionHandle());			
}


//������� ��������� ������ ������
static void ble_rx_task_function (void * pvParameter)
{
    UNUSED_PARAMETER(pvParameter);
		BleTransactDataTypeDef *transactData = (BleTransactDataTypeDef*)pvParameter;
    while (true)
    {
			TryGetDataFromRxQueue(RxQueueElementHandler);
			if(!IsConnected)
			{
				vTaskSuspend(NULL);
			}
			//NRF_LOG_FLUSH();
			taskYIELD();
			//nrf_pwr_mgmt_run();
    }
}

static void hum_task_function (void * pvParameter)
{
    while (true)
    {
			float temperature,humidity;
			uint16_t len = sizeof(float)*2;
			uint8_t dataHum[len];
			ControlTemperatureSensor(true);
			vTaskDelay(700);	
			uint8_t measResult;
			if( xSemaphoreTake( xSemaphoreHum, ( TickType_t ) 100000 ) == pdTRUE )
      {
				measResult = GetTemperatureAndHumidity(&temperature,&humidity);
        xSemaphoreGive( xSemaphoreHum );
      }			
			
			ControlTemperatureSensor(false);
			if(measResult == 0)
			{
					memcpy(dataHum,&temperature,sizeof(float));
					memcpy(dataHum + sizeof(float),&humidity,sizeof(float));
					hum_airdevice_data_send(BleTransactData.pAirDevInstance,dataHum,&len,GetConnectionHandle());
			}
			if(!IsConnected)
			{
				vTaskSuspend(NULL);
			}
			
			vTaskDelay(1000);
			//nrf_pwr_mgmt_run();
    }
}

//static double voltage;
//������� ��������� ������ ��������


static void wake_up_task_function (void * pvParameter)
{
	AirDeviceConfigTypeDef *config = (AirDeviceConfigTypeDef*)pvParameter;
  UNUSED_PARAMETER(pvParameter);		
  while (true)
  {	
		HandleWakeUpTask(&AirDeviceConfig);
		vTaskDelay(config->WakeUpTimeSec * 1000);
  }
}

//���������� ������ � ������� ������ �� BLE
BaseType_t PutDataToRxQueue(const uint8_t *data,uint16_t len)
{
	if(len > GetMaxDataLen())
	{
		return pdFAIL;
	}
	
	BleQueueItemTypeDef queueItem;
	BaseType_t result = xQueueReceive(BleTransactData.RxEmptyItemsQueue,&queueItem,10);
	
	if(result != pdPASS)
	{
		return result;
	}
	
	if(uxQueueMessagesWaiting(BleTransactData.RxFullItemsQueue) < QUEUE_RX_MAX_LEN)
	{
		queueItem.ItemSize = len;
		memcpy(queueItem.pItem,data,queueItem.ItemSize);
		xQueueSend(BleTransactData.RxFullItemsQueue, &queueItem, 10);
		return pdPASS;
	}
	else
	{
		return pdFAIL;
	}

}

//���������� ������ � ������� �������� �� BLE (���������� �-��� SendBleData)
BaseType_t SendBleData(uint8_t *data,uint16_t len)
{
	if(!IsConnected)
	{
		return pdPASS;
	}
	TxElementHandler(data,len);
																												
	return pdPASS;		
}

//��������� ������ �� ������� ������ �� BLE
BaseType_t TryGetDataFromRxQueue(QueueItemHandlerTypeDef itemHandleCallBack)
{
	BaseType_t result = pdFAIL;
	
  if(itemHandleCallBack == NULL)																						//�������� ������� �-��� ��������� ������
	{
		return result;
	}
	
	if(uxQueueMessagesWaiting(BleTransactData.RxFullItemsQueue) > 0)
	{
		BleQueueItemTypeDef queueItem;
		xQueueReceive(BleTransactData.RxFullItemsQueue,&queueItem,10);
		itemHandleCallBack(queueItem.pItem,queueItem.ItemSize);
		xQueueSend(BleTransactData.RxEmptyItemsQueue,&queueItem,10);
		result = pdPASS;
	}

	return result;		
}



void SuspendAllTasks(void)
{
	//vTaskSuspend(ble_tx_task_handle);
	
}


void ResumeConnectionTasks(void)
{
	
	vTaskResume(ble_rx_task_handle);
	vTaskResume(hum_task_handle);
}

void SuspendWakeUpTasks(void)
{
	vTaskSuspend(wake_up_task_handle);
//	vTaskResume(ble_rx_task_handle);
//	vTaskResume(hum_task_handle);
}

void ResumeWakeUpTasks(void)
{
	vTaskResume(wake_up_task_handle);

}

void AddDumperPosToQueue(DumperPositionType position,JournalEventTriggerType trigger)
{
	if(uxQueueMessagesWaiting(DumperPosQueue) < DUMPER_POS_QUEUE_LEN)
	{
		DumperPositionItemTypeDef item;
		item.PosType = position;
		item.Trigger = trigger;
		xQueueSend(DumperPosQueue,&item,NULL);
	}
	TaskStatus_t xTaskDetails;
	vTaskGetInfo( /* The handle of the task being queried. */
                  dumper_task_handle,
                  /* The TaskStatus_t structure to complete with information
                  on xTask. */
                  &xTaskDetails,
                  /* Include the stack high water mark value in the
                  TaskStatus_t structure. */
                  pdTRUE,
                  /* Include the task state in the TaskStatus_t structure. */
                  eInvalid );
	
	if(xTaskDetails.eCurrentState == eSuspended)
	{
		vTaskResume(dumper_task_handle);
	}
}


void OnStartFirmawareUpdate(void)
{
	vTaskSuspend(hum_task_handle);
	vTaskSuspend(wake_up_task_handle);
	vTaskSuspend(dumper_task_handle);
}

