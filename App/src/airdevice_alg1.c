#include "airdevice_alg1.h"

static void HandleWakeUpEvent(void);
static bool HandleDumperPartlyPosition(AirDeviceConfigTypeDef *config, uint8_t pos);
static DumperError HandleDumperPosQueueItem(AirDeviceConfigTypeDef *config,DumperPositionItemTypeDef item);
static void CheckTemperatureAndHumidity(AirDeviceConfigTypeDef *config);
static double GetMod(double difOne, double difTwo);

volatile static float HumMeasures[HUM_TEMP_MEASURE_NUM] = {0};
volatile static float TempMeasures[HUM_TEMP_MEASURE_NUM] = {0};
static uint16_t MeasuresNum = 0;
static bool IsReadyToCompareHum = false;


void OnDeviceConnect(void)
{
	ResumeConnectionTasks();
}

void OnDeviceDisconnect(void)
{
	
	ResetRxQueues();
	//nrf_drv_rtc_enable(&wake_up_rtc);
}

static void HandleWakeUpEvent(void)
{
	
}




void HandleWakeUpTask(AirDeviceConfigTypeDef *config)
{
	if(!IsConnected)
	{
		ControlLed(true);
		vTaskDelay(150);
		ControlLed(false);
	
	//�������� ������������ ����, ��������� �������
		vTaskDelay(500);
		CheckTemperatureAndHumidity(config);	
	}


	

	//NRF_LOG_INFO("Test.");
	//NRF_LOG_PROCESS();

}

void HandleSetWakeUpIntervalCommand(uint8_t * data, uint32_t len, AirDeviceConfigTypeDef *config)
{
	if(len != sizeof(uint16_t))
	{
		return;
	}
	uint16_t wakeUpInterval = *((uint16_t*)data);
	uint8_t txBleBuf[2];
	if(wakeUpInterval == 0)
	{
		
		txBleBuf[0] = (uint8_t)CMD_RESP_MARKER_WAKEUP_INTERVAL;
		txBleBuf[1] = (uint8_t)CMD_RESP_ERR_WAKEUP_INTERVAL_WRONG_VAL_ERROR;
		SendBleData(txBleBuf,2);
		return;
	}
	
	config->WakeUpTimeSec = wakeUpInterval;

	AirDeviceResponseEnum resp = SaveAirDeviceConfigToEEPROM(config) == 0 ?
	CMD_RESP_OK : CMD_RESP_ERR_WAKEUP_INTERVAL_SAVE_ERROR;	
	
	txBleBuf[0] = (uint8_t)CMD_RESP_MARKER_WAKEUP_INTERVAL;
	txBleBuf[1] = (uint8_t)resp;
	SendBleData(txBleBuf,2);
	
}

void HandleGetWakeUpIntervalCommand(AirDeviceConfigTypeDef *config)
{
	uint8_t txBleBuf[3];
	txBleBuf[0] = (uint8_t)CMD_RESP_MARKER_WAKEUP_INTERVAL;
	txBleBuf[1] = (uint8_t)config->WakeUpTimeSec;
	txBleBuf[2] = (uint8_t)(config->WakeUpTimeSec >> 8);
	SendBleData(txBleBuf,3);
}


void dumper_task_function(void * pvParameter)
{
	AirDeviceConfigTypeDef *config = (AirDeviceConfigTypeDef*)pvParameter;
	if(!config->IsDumperCalibrated)
	{
		AddDumperPosToQueue(DUMPER_POS_CALIBRATION, JEVENT_TRIGGER_SELF);
	}
	else
	{
#ifdef ALG_DEMO
		AddDumperPosToQueue(DUMPER_POS_FULL_CLOSE, JEVENT_TRIGGER_SELF);
		//vTaskSuspend(NULL);
#else
		vTaskSuspend(NULL);
#endif
	}	
	
  while (true)
  {	
		if(uxQueueMessagesWaiting(DumperPosQueue) > 0)
		{			
			DumperPositionItemTypeDef item;
			if(xQueueReceive(DumperPosQueue, &item, NULL) == pdTRUE)
			{
				ControlBatteryAdcDivider(true);
				vTaskDelay(100);
				HandleDumperPosQueueItem(config, item);
				ControlBatteryAdcDivider(false);
			}
		}
		else
		{
			vTaskSuspend(NULL);
		}

  }
}

static DumperError HandleDumperPosQueueItem(AirDeviceConfigTypeDef *config,DumperPositionItemTypeDef item)
{
	switch(item.PosType)
	{
		case DUMPER_POS_IDLE:
		{
			
			break;
		}
		case DUMPER_POS_INVALID:
		{
			
			break;
		}
		case DUMPER_POS_CALIBRATION:
		{
			uint8_t txBleData[2];
			txBleData[0] = CMD_RESP_MARKER_CALIBRATION;

			for(uint8_t cnt=0;cnt < 5; cnt++)
			{
				double full_open_mul = 0;
				double full_close_adc_mul = 0;
			
				Control5VoltDcDc(true);
				Control5VoltPower(true);
				ControlBatteryAdcDivider(true);
				StartMotor(false);
				vTaskDelay(500);
				full_open_mul = MeasureEncoderAdcValWhileChanged();
				StopMotor();
				vTaskDelay(100);
				Control5VoltDcDc(true);
				Control5VoltPower(true);
				StartMotor(true);
				vTaskDelay(500);
				full_close_adc_mul = MeasureEncoderAdcValWhileChanged();
				StopMotor();
				Control5VoltDcDc(false);
				Control5VoltPower(false);

				if(full_open_mul > full_close_adc_mul)
				{
					double difGapFullOpen = (full_open_mul - full_close_adc_mul) * DUMPER_GAP_MUL_FULL_OPEN;						
					double difGapFullClose = (full_open_mul - full_close_adc_mul) * DUMPER_GAP_MUL_FULL_CLOSE;
					
					config->DumperFullOpenAdcMul = full_open_mul - difGapFullOpen;
					config->DumperFullCloseAdcMul = full_close_adc_mul + difGapFullClose;
					config->IsDumperCalibrated = true;
					if(SaveAirDeviceConfigToEEPROM(config) == 0)
					{
						
						txBleData[1] = CMD_RESP_OK;
						SendBleData(txBleData,sizeof(txBleData));
						return DUMPER_ERROR_NONE;
					}
				}				
			}
			txBleData[1] = CMD_RESP_ERR_CALIBRATION;
			SendBleData(txBleData,sizeof(txBleData));
			return DUMPER_ERROR_CALIBRATION;
		}
		case DUMPER_POS_FULL_OPEN:
		{
			double vcc_voltage;
			double enc_voltage;

			bool isTimeOut = false;
			uint8_t txBleData[2];
			txBleData[0] = CMD_RESP_MARKER_DAMPER;
			
			GetAveragedVccAndEncVoltage(&vcc_voltage,&enc_voltage);
			volatile double absDif = GetMod(enc_voltage*config->DumperFullOpenAdcMul, vcc_voltage);
			if(enc_voltage*config->DumperFullOpenAdcMul <= vcc_voltage || absDif < ADS_DIF_THR)
			{
				txBleData[1] = CMD_RESP_OK;		
				SendBleData(txBleData,sizeof(txBleData));
				return isTimeOut;
			}
			
			Control5VoltDcDc(true);
			Control5VoltPower(true);
			StartMotor(false);
			StartTimeoutTimer(DUMPER_MOVE_TIMEOUT_SEC);
			vTaskDelay(50);
			CheckBatteryVoltage();
			
			do
			{
				GetAveragedVccAndEncVoltage(&vcc_voltage,&enc_voltage);
				
				if(CheckTimeout())
				{
					isTimeOut = true;
					break;
				}
			}			
			while(enc_voltage*config->DumperFullOpenAdcMul > vcc_voltage);
			
			StopMotor();
	
			StopTimeoutTimer();
			txBleData[1] = isTimeOut ? CMD_RESP_ERR_DUMPER_SET : CMD_RESP_OK;		
			
			SendBleData(txBleData,sizeof(txBleData));
			
			SaveEventToJournal(item.Trigger,isTimeOut ? JEVENT_FAULT_MOVE_TIMEOUT :  JEVENT_MOVE_FULL_OPEN);
			
			break;
		}	
		case DUMPER_POS_FULL_CLOSE:
		{
			double vcc_voltage;
			double enc_voltage;

			bool isTimeOut = false;
			uint8_t txBleData[2];
			txBleData[0] = CMD_RESP_MARKER_DAMPER;
			
			GetAveragedVccAndEncVoltage(&vcc_voltage,&enc_voltage);
			volatile double absDif = GetMod(enc_voltage*config->DumperFullCloseAdcMul, vcc_voltage);
			if(enc_voltage*config->DumperFullCloseAdcMul >= vcc_voltage  || absDif < ADS_DIF_THR)
			{
				txBleData[1] = CMD_RESP_OK;		
				SendBleData(txBleData,sizeof(txBleData));
				return isTimeOut;
			}		
			
			Control5VoltDcDc(true);
			Control5VoltPower(true);
			StartMotor(true);
			StartTimeoutTimer(DUMPER_MOVE_TIMEOUT_SEC);
			vTaskDelay(50);
			CheckBatteryVoltage();
			
			do
			{
				GetAveragedVccAndEncVoltage(&vcc_voltage,&enc_voltage);

				if(CheckTimeout())
				{
					isTimeOut = true;
					break;
				}
			}			
			while(enc_voltage*config->DumperFullCloseAdcMul < vcc_voltage);
			
			StopMotor();	
	
			StopTimeoutTimer();
			txBleData[1] = isTimeOut ? CMD_RESP_ERR_DUMPER_SET : CMD_RESP_OK;		

			SendBleData(txBleData,sizeof(txBleData));
			SaveEventToJournal(item.Trigger,isTimeOut ? JEVENT_FAULT_MOVE_TIMEOUT :  JEVENT_MOVE_FULL_CLOSE);			
			break;
		}
		case DUMPER_POS_PARTLY_1_6:
		{
			bool isTimeOut = HandleDumperPartlyPosition(config, 1);
			SaveEventToJournal(item.Trigger,isTimeOut ? JEVENT_FAULT_MOVE_TIMEOUT :  JEVENT_MOVE_1_6);		
			break;
		}
		case DUMPER_POS_PARTLY_2_6:
		{
			bool isTimeOut = HandleDumperPartlyPosition(config, 2);
			SaveEventToJournal(item.Trigger,isTimeOut ? JEVENT_FAULT_MOVE_TIMEOUT :  JEVENT_MOVE_2_6);			
			break;
		}
		case DUMPER_POS_PARTLY_3_6:
		{
			bool isTimeOut = HandleDumperPartlyPosition(config, 3);
			SaveEventToJournal(item.Trigger,isTimeOut ? JEVENT_FAULT_MOVE_TIMEOUT :  JEVENT_MOVE_3_6);			
			break;
		}
		case DUMPER_POS_PARTLY_4_6:
		{
			bool isTimeOut = HandleDumperPartlyPosition(config, 4);	
			SaveEventToJournal(item.Trigger,isTimeOut ? JEVENT_FAULT_MOVE_TIMEOUT :  JEVENT_MOVE_4_6);			
			break;
		}
		case DUMPER_POS_PARTLY_5_6:
		{
			bool isTimeOut = HandleDumperPartlyPosition(config, 5);	
			SaveEventToJournal(item.Trigger,isTimeOut ? JEVENT_FAULT_MOVE_TIMEOUT :  JEVENT_MOVE_5_6);
			break;
		}

		default:
			break;
	}
	return DUMPER_ERROR_UNKNOWN;
}


static double GetMod(double difOne, double difTwo)
{
	return fabs(difOne - difTwo);
}


void HandleDumperCalibrationCommand(JournalEventTriggerType trigger)
{
	AddDumperPosToQueue(DUMPER_POS_CALIBRATION, trigger);
}



static bool HandleDumperPartlyPosition(AirDeviceConfigTypeDef *config, uint8_t pos)
{
	double vcc_voltage, enc_voltage;

	
	bool isTimeOut = false;
	uint8_t txBleData[2] = {CMD_RESP_MARKER_DAMPER, CMD_RESP_OK};

	/*������� ������� ����� ���������� �������� (�������) � �������� (�������), ��������� ����� �� ������� ����� ���������� �������� �� 
	�������, ����� ������������ � ��������� ��������
	*/
	double target_mul = (((config->DumperFullOpenAdcMul - config->DumperFullCloseAdcMul)/6) * pos) + config->DumperFullCloseAdcMul;

	
	GetAveragedVccAndEncVoltage(&vcc_voltage,&enc_voltage);
	double current_mul = vcc_voltage / enc_voltage;
	

	if(current_mul > target_mul)
	{
		volatile double absDif = GetMod(enc_voltage*target_mul, vcc_voltage);
		if(enc_voltage*target_mul >= vcc_voltage  || absDif < ADS_DIF_THR)
		{			
			txBleData[1] = CMD_RESP_OK;
			SendBleData(txBleData,sizeof(txBleData));
			return isTimeOut;
		}
		

		Control5VoltDcDc(true);
		Control5VoltPower(true);
		StartMotor(true);
		StartTimeoutTimer(DUMPER_MOVE_TIMEOUT_SEC);
		CheckBatteryVoltage();

		do
		{
			GetAveragedVccAndEncVoltage(&vcc_voltage,&enc_voltage);
			

			if(CheckTimeout())
			{
				isTimeOut = true;
				break;
			}
		}			
		while(enc_voltage*target_mul < vcc_voltage);
			
		StopMotor();

		StopTimeoutTimer();
		txBleData[1] = isTimeOut ? CMD_RESP_ERR_DUMPER_SET : CMD_RESP_OK;		

		
	}
	else if(current_mul < target_mul)
	{
		volatile double absDif = GetMod(enc_voltage*target_mul, vcc_voltage);
		if(enc_voltage*target_mul <= vcc_voltage  || absDif < ADS_DIF_THR)
		{			
			txBleData[1] = CMD_RESP_OK;
			SendBleData(txBleData,sizeof(txBleData));
			return isTimeOut;
		}

		Control5VoltDcDc(true);
		Control5VoltPower(true);
		StartMotor(false);
		StartTimeoutTimer(DUMPER_MOVE_TIMEOUT_SEC);
		CheckBatteryVoltage();

		do
		{
			GetAveragedVccAndEncVoltage(&vcc_voltage,&enc_voltage);

			if(CheckTimeout())
			{
				isTimeOut = true;
				break;
			}
		}			
		while(enc_voltage*target_mul > vcc_voltage);
			
		StopMotor();	

		StopTimeoutTimer();
		txBleData[1] = isTimeOut ? CMD_RESP_ERR_DUMPER_SET : CMD_RESP_OK;				
	}
	
	SendBleData(txBleData,sizeof(txBleData));
	return isTimeOut;	

}



uint16_t MeasInterval = 0;
static void CheckTemperatureAndHumidity(AirDeviceConfigTypeDef *config)
{
	float temperature,humidity;
	double vcc_voltage, enc_voltage;
	uint8_t measResult;
	ControlTemperatureSensor(true);
	vTaskDelay(150);
	if( xSemaphoreTake( xSemaphoreHum, ( TickType_t ) 100000 ) == pdTRUE )
  {
		measResult = GetTemperatureAndHumidity(&temperature,&humidity);
    xSemaphoreGive( xSemaphoreHum );
  }	
	ControlTemperatureSensor(false);
#ifdef ALG_DEMO
	MeasuresNum = MeasuresNum < HUM_TEMP_MEASURE_NUM ? (MeasuresNum + 1) : 1;
	HumMeasures[MeasuresNum - 1] = humidity;
	TempMeasures[MeasuresNum - 1] = temperature;	
	
	if(!IsReadyToCompareHum && (MeasuresNum >= HUM_TEMP_MEASURE_NUM))
	{
		IsReadyToCompareHum = true;
	}
	
	if(IsReadyToCompareHum)
	{
		float lastMeas = HumMeasures[MeasuresNum - 1];
		float cmpMeas = MeasuresNum < HUM_TEMP_MEASURE_NUM  ? HumMeasures[MeasuresNum] : HumMeasures[0];
		if((lastMeas - cmpMeas) >= HUM_THR_FOR_OPEN_PERCENT)
		{
			ControlBatteryAdcDivider(true);
			vTaskDelay(100);
			GetAveragedVccAndEncVoltage(&vcc_voltage,&enc_voltage);
			ControlBatteryAdcDivider(false);
			if(enc_voltage*config->DumperFullOpenAdcMul > vcc_voltage)
			{
				AddDumperPosToQueue(DUMPER_POS_FULL_OPEN,JEVENT_TRIGGER_SENSOR);
			}
			//printf("Open");
		}
		else if(cmpMeas - lastMeas)
		{
			ControlBatteryAdcDivider(true);
			vTaskDelay(100);
			GetAveragedVccAndEncVoltage(&vcc_voltage,&enc_voltage);
			ControlBatteryAdcDivider(false);
			if(enc_voltage*config->DumperFullCloseAdcMul < vcc_voltage)
			{
				AddDumperPosToQueue(DUMPER_POS_FULL_CLOSE,JEVENT_TRIGGER_SENSOR);
			}
			//printf("Close");
		}
	}
	
	if(MeasInterval < SAVE_MEASURE_INTERVAL)
	{
		MeasInterval++;
	}
	else
	{
		SaveMeasuresToJournal(temperature, humidity);
		MeasInterval = 0;
	}
	
#endif
	
}
