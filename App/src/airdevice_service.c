
#include "sdk_common.h"
#include "ble.h"
#include "airdevice_service.h"
#include "ble_srv_common.h"


#include "nrf_log.h"
#include "main.h"


#define BLE_UUID_AIRDEVICE_TX_CHARACTERISTIC 0x0003               /**< The UUID of the TX Characteristic. */
#define BLE_UUID_AIRDEVICE_RX_CHARACTERISTIC 0x0002               /**< The UUID of the RX Characteristic. */

#define BLE_AIRDEVICE_MAX_CHAR_LEN        BLE_AIRDEVICE_MAX_DATA_LEN /**< Maximum length of the RX Characteristic (in bytes). */





/**@brief Function for handling the @ref BLE_GAP_EVT_CONNECTED event from the SoftDevice.
 *
 * @param[in] p_airdevice     Nordic UART Service structure.
 * @param[in] p_ble_evt Pointer to the event received from BLE stack.
 */
static void on_connect(ble_airdevice_t * p_airdevice, ble_evt_t const * p_ble_evt)
{
    ret_code_t                 err_code;
    ble_airdevice_evt_t              evt;
    ble_gatts_value_t          gatts_val;
    uint8_t                    cccd_value[2];
    ble_airdevice_client_context_t * p_client = NULL;

    err_code = blcm_link_ctx_get(p_airdevice->p_link_ctx_storage,
                                 p_ble_evt->evt.gap_evt.conn_handle,
                                 (void *) &p_client);
    if (err_code != NRF_SUCCESS)
    {
        NRF_LOG_ERROR("Link context for 0x%02X connection handle could not be fetched.",
                      p_ble_evt->evt.gap_evt.conn_handle);
    }

    /* Check the hosts CCCD value to inform of readiness to send data using the RX characteristic */
    memset(&gatts_val, 0, sizeof(ble_gatts_value_t));
    gatts_val.p_value = cccd_value;
    gatts_val.len     = sizeof(cccd_value);
    gatts_val.offset  = 0;

    err_code = sd_ble_gatts_value_get(p_ble_evt->evt.gap_evt.conn_handle,
                                      p_airdevice->tx_handles.cccd_handle,
                                      &gatts_val);

    if ((err_code == NRF_SUCCESS)     &&
        (p_airdevice->data_handler != NULL) &&
        ble_srv_is_notification_enabled(gatts_val.p_value))
    {
        if (p_client != NULL)
        {
            p_client->is_notification_enabled = true;
        }

        memset(&evt, 0, sizeof(ble_airdevice_evt_t));
        evt.type        = BLE_AIRDEVICE_EVT_COMM_STARTED;
        evt.p_airdevice       = p_airdevice;
        evt.conn_handle = p_ble_evt->evt.gap_evt.conn_handle;
        evt.p_link_ctx  = p_client;

        p_airdevice->data_handler(&evt);
    }
}


/**@brief Function for handling the @ref BLE_GATTS_EVT_WRITE event from the SoftDevice.
 *
 * @param[in] p_airdevice     Nordic UART Service structure.
 * @param[in] p_ble_evt Pointer to the event received from BLE stack.
 */
static void on_write(ble_airdevice_t * p_airdevice, ble_evt_t const * p_ble_evt)
{
    ret_code_t                    err_code;
    ble_airdevice_evt_t                 evt;
    ble_airdevice_client_context_t    * p_client;
    ble_gatts_evt_write_t const * p_evt_write = &p_ble_evt->evt.gatts_evt.params.write;

    err_code = blcm_link_ctx_get(p_airdevice->p_link_ctx_storage,
                                 p_ble_evt->evt.gatts_evt.conn_handle,
                                 (void *) &p_client);
    if (err_code != NRF_SUCCESS)
    {
        NRF_LOG_ERROR("Link context for 0x%02X connection handle could not be fetched.",
                      p_ble_evt->evt.gatts_evt.conn_handle);
    }

    memset(&evt, 0, sizeof(ble_airdevice_evt_t));
    evt.p_airdevice       = p_airdevice;
    evt.conn_handle = p_ble_evt->evt.gatts_evt.conn_handle;
    evt.p_link_ctx  = p_client;

    if ((p_evt_write->handle == p_airdevice->tx_handles.cccd_handle) &&
        (p_evt_write->len == 2))
    {
        if (p_client != NULL)
        {
            if (ble_srv_is_notification_enabled(p_evt_write->data))
            {
                p_client->is_notification_enabled = true;
                evt.type                          = BLE_AIRDEVICE_EVT_COMM_STARTED;
            }
            else
            {
                p_client->is_notification_enabled = false;
                evt.type                          = BLE_AIRDEVICE_EVT_COMM_STOPPED;
            }

            if (p_airdevice->data_handler != NULL)
            {
                p_airdevice->data_handler(&evt);
            }

        }
    }
    else if ((p_evt_write->handle == p_airdevice->rx_handles.value_handle) &&
             (p_airdevice->data_handler != NULL))
    {
        evt.type                  = BLE_AIRDEVICE_EVT_DATA_RX;
        evt.params.rx_data.p_data = p_evt_write->data;
        evt.params.rx_data.length = p_evt_write->len;

        p_airdevice->data_handler(&evt);
    }
    else
    {
        // Do Nothing. This event is not relevant for this service.
    }
}


/**@brief Function for handling the @ref BLE_GATTS_EVT_HVN_TX_COMPLETE event from the SoftDevice.
 *
 * @param[in] p_airdevice     Nordic UART Service structure.
 * @param[in] p_ble_evt Pointer to the event received from BLE stack.
 */
static void on_hvx_tx_complete(ble_airdevice_t * p_airdevice, ble_evt_t const * p_ble_evt)
{
    ret_code_t                 err_code;
    ble_airdevice_evt_t              evt;
    ble_airdevice_client_context_t * p_client;

    err_code = blcm_link_ctx_get(p_airdevice->p_link_ctx_storage,
                                 p_ble_evt->evt.gatts_evt.conn_handle,
                                 (void *) &p_client);
    if (err_code != NRF_SUCCESS)
    {
        NRF_LOG_ERROR("Link context for 0x%02X connection handle could not be fetched.",
                      p_ble_evt->evt.gatts_evt.conn_handle);
        return;
    }

    if (p_client->is_notification_enabled)
    {
        memset(&evt, 0, sizeof(ble_airdevice_evt_t));
        evt.type        = BLE_AIRDEVICE_EVT_TX_RDY;
        evt.p_airdevice       = p_airdevice;
        evt.conn_handle = p_ble_evt->evt.gatts_evt.conn_handle;
        evt.p_link_ctx  = p_client;

        p_airdevice->data_handler(&evt);
    }
}


void ble_airdevice_on_ble_evt(ble_evt_t const * p_ble_evt, void * p_context)
{
    if ((p_context == NULL) || (p_ble_evt == NULL))
    {
        return;
    }

    ble_airdevice_t * p_airdevice = (ble_airdevice_t *)p_context;

    switch (p_ble_evt->header.evt_id)
    {
        case BLE_GAP_EVT_CONNECTED:
            on_connect(p_airdevice, p_ble_evt);
            break;

        case BLE_GATTS_EVT_WRITE:
            on_write(p_airdevice, p_ble_evt);
            break;

        case BLE_GATTS_EVT_HVN_TX_COMPLETE:
            on_hvx_tx_complete(p_airdevice, p_ble_evt);
            break;

        default:
            // No implementation needed.
            break;
    }
}


uint32_t ble_airdevice_init(ble_airdevice_t * p_airdevice, ble_airdevice_init_t const * p_airdevice_init)
{
    ret_code_t            err_code;
    ble_uuid_t            ble_uuid;
    ble_uuid128_t         airdevice_base_uuid = AIRDEVICE_SERVICE_UUID_128;
    ble_add_char_params_t add_char_params;

    VERIFY_PARAM_NOT_NULL(p_airdevice);
    VERIFY_PARAM_NOT_NULL(p_airdevice_init);

    // Initialize the service structure.
    p_airdevice->data_handler = p_airdevice_init->data_handler;
		
    /**@snippet [Adding proprietary Service to the SoftDevice] */
    // Add a custom base UUID.
    err_code = sd_ble_uuid_vs_add(&airdevice_base_uuid, &p_airdevice->uuid_type);
    VERIFY_SUCCESS(err_code);

    ble_uuid.type = BLE_UUID_TYPE_BLE; //BLE_UUID_TYPE_BLE
    ble_uuid.uuid = AIRDEVICE_SERVICE_UUID;

    // Add the service.
    err_code = sd_ble_gatts_service_add(BLE_GATTS_SRVC_TYPE_PRIMARY,
                                        &ble_uuid,
                                        &p_airdevice->service_handle);
    /**@snippet [Adding proprietary Service to the SoftDevice] */
    VERIFY_SUCCESS(err_code);

//    // Add the RX Characteristic.
//    memset(&add_char_params, 0, sizeof(add_char_params));
//    add_char_params.uuid                     = AIRDEVICE_SERVICE_UUID;
//    add_char_params.uuid_type                = p_airdevice->uuid_type;
//    add_char_params.max_len                  = BLE_AIRDEVICE_MAX_RX_CHAR_LEN;
//    add_char_params.init_len                 = sizeof(uint8_t);
//    add_char_params.is_var_len               = true;
//    add_char_params.char_props.write         = 1;
//    add_char_params.char_props.write_wo_resp = 1;

//    add_char_params.read_access  = SEC_OPEN;
//    add_char_params.write_access = SEC_OPEN;

//    err_code = characteristic_add(p_airdevice->service_handle, &add_char_params, &p_airdevice->rx_handles);
//    if (err_code != NRF_SUCCESS)
//    {
//        return err_code;
//    }

    // Add the Characteristic.
    
    memset(&add_char_params, 0, sizeof(add_char_params));
    add_char_params.uuid              = AIRDEVICE_MAIN_CHARACTERISTIC_UUID;
    add_char_params.uuid_type         = BLE_UUID_TYPE_BLE;
    add_char_params.max_len           = 240;
    add_char_params.init_len          = sizeof(uint8_t);
    add_char_params.is_var_len        = true;
    add_char_params.char_props.notify = 1;
		add_char_params.char_props.write_wo_resp = 1;	
		//add_char_params.char_props.write = 1;	
		
    add_char_params.read_access       = SEC_OPEN;
    add_char_params.write_access      = SEC_OPEN;
    add_char_params.cccd_write_access = SEC_OPEN; //AIRDEVICE_HUM_CHARACTERISTIC_UUID
		
		
		VERIFY_SUCCESS(characteristic_add(p_airdevice->service_handle, &add_char_params, &p_airdevice->rx_handles));
    
    memset(&add_char_params, 0, sizeof(add_char_params));
    add_char_params.uuid              = AIRDEVICE_HUM_CHARACTERISTIC_UUID;
    add_char_params.uuid_type         = BLE_UUID_TYPE_BLE;
    add_char_params.max_len           = 16;
    add_char_params.init_len          = sizeof(uint8_t);
    add_char_params.is_var_len        = true;
    add_char_params.char_props.notify = 1;
		//add_char_params.char_props.write_wo_resp = 1;	
		//add_char_params.char_props.write = 1;	
		
    add_char_params.read_access       = SEC_OPEN;
    add_char_params.write_access      = SEC_OPEN;
    add_char_params.cccd_write_access = SEC_OPEN; //		
		
		
		return characteristic_add(p_airdevice->service_handle, &add_char_params, &p_airdevice->hum_handles);
    
}

uint32_t hum_airdevice_data_send(ble_airdevice_t * p_airdevice,
                           uint8_t   * p_data,
                           uint16_t  * p_length,
                           uint16_t    conn_handle)
{
    ret_code_t                 err_code;
    ble_gatts_hvx_params_t     hvx_params;
    ble_airdevice_client_context_t * p_client;

    VERIFY_PARAM_NOT_NULL(p_airdevice);

    err_code = blcm_link_ctx_get(p_airdevice->p_link_ctx_storage, conn_handle, (void *) &p_client);
    VERIFY_SUCCESS(err_code);

    if ((conn_handle == BLE_CONN_HANDLE_INVALID) || (p_client == NULL))
    {
        return NRF_ERROR_NOT_FOUND;
    }

    if (*p_length > BLE_AIRDEVICE_MAX_DATA_LEN)
    {
        return NRF_ERROR_INVALID_PARAM;
    }

    memset(&hvx_params, 0, sizeof(hvx_params));

    hvx_params.handle = p_airdevice->hum_handles.value_handle;
    hvx_params.p_data = p_data;
    hvx_params.p_len  = p_length;
    hvx_params.type   = BLE_GATT_HVX_NOTIFICATION;
		uint32_t err = sd_ble_gatts_hvx(conn_handle, &hvx_params);
    return err;
}


uint32_t ble_airdevice_data_send(ble_airdevice_t * p_airdevice,
                           uint8_t   * p_data,
                           uint16_t  * p_length,
                           uint16_t    conn_handle)
{
    ret_code_t                 err_code;
    ble_gatts_hvx_params_t     hvx_params;
    ble_airdevice_client_context_t * p_client;

    VERIFY_PARAM_NOT_NULL(p_airdevice);

    err_code = blcm_link_ctx_get(p_airdevice->p_link_ctx_storage, conn_handle, (void *) &p_client);
    VERIFY_SUCCESS(err_code);

    if ((conn_handle == BLE_CONN_HANDLE_INVALID) || (p_client == NULL))
    {
        return NRF_ERROR_NOT_FOUND;
    }

    if (*p_length > BLE_AIRDEVICE_MAX_DATA_LEN)
    {
        return NRF_ERROR_INVALID_PARAM;
    }

    memset(&hvx_params, 0, sizeof(hvx_params));

    hvx_params.handle = p_airdevice->rx_handles.value_handle;
    hvx_params.p_data = p_data;
    hvx_params.p_len  = p_length;
    hvx_params.type   = BLE_GATT_HVX_NOTIFICATION;//NRF_ERROR_INVALID_STATE
		uint32_t counter = 0;
		while(sd_ble_gatts_hvx(conn_handle, &hvx_params) != NRF_SUCCESS)
		{
			counter++;
			if(counter > MAX_TX_COUNTER_THR)
			{
				break;
			}
		}
    return 0;
}



