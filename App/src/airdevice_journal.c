#include "airdevice_journal.h"


void SaveEventToJournal(JournalEventTriggerType trigger, JournalEventType event)
{
	if(IsDateTimeSynchronized() && AirDeviceConfig.IsJournalEnabled)
	{
		JournalEventItemTypeDef item;
		item.Event = event;
		item.Trigger = trigger;
		item.Time = GetCurrentTimeSec();
		
		uint16_t event_items_last_pos; 
		At25Rd((uint8_t*)&event_items_last_pos, AT25256_DATA_COUNTERS_AREA_OFFSET,sizeof(uint16_t));
		//��������, ��� ���������� �������� �������� �� ��������� ����� �������� ��������� �������. ���� ��������� - ��������
		event_items_last_pos = event_items_last_pos < MAX_SAVED_EVENTS_NUM  ? event_items_last_pos : 0;
		
		At25Wr((uint8_t*)&item, AT25256_EVENT_AREA_OFFSET + (sizeof(JournalEventItemTypeDef)-2)*event_items_last_pos,(sizeof(JournalEventItemTypeDef)-2));
		
		event_items_last_pos++;
		
		At25Wr((uint8_t*)&event_items_last_pos, AT25256_DATA_COUNTERS_AREA_OFFSET,sizeof(uint16_t));
	}
}

void SaveMeasuresToJournal(float temperature, float humidity)
{
	if(IsDateTimeSynchronized() && AirDeviceConfig.IsJournalEnabled)
	{
		JournalMeasureItemTypeDef item;
		item.Temperature = temperature;
		item.Humidity = humidity;
		item.Time = GetCurrentTimeSec();
		
		uint16_t event_items_last_pos; 
		At25Rd((uint8_t*)&event_items_last_pos, AT25256_DATA_COUNTERS_AREA_OFFSET + sizeof(uint16_t),sizeof(uint16_t));
		//��������, ��� ���������� �������� �������� �� ��������� ����� �������� ��������� �������. ���� ��������� - ��������
		event_items_last_pos = event_items_last_pos < MAX_SAVED_MEASURES_NUM  ? event_items_last_pos : 0;
		
		At25Wr((uint8_t*)&item, AT25256_MEASURE_AREA_OFFSET + sizeof(JournalMeasureItemTypeDef)*event_items_last_pos,sizeof(JournalMeasureItemTypeDef));
	
		event_items_last_pos++;		
	
		At25Wr((uint8_t*)&event_items_last_pos, AT25256_DATA_COUNTERS_AREA_OFFSET + sizeof(uint16_t),sizeof(uint16_t));
	}	
}

void HandleSetEnDisJournalCommand(AirDeviceConfigTypeDef *config, uint8_t * data, uint32_t len)
{
	if(len != sizeof(uint8_t))
	{
		return;
	}	
	
	uint8_t txBleBuf[2];
	
	config->IsJournalEnabled = data[0] > 0 ? true : false;

	AirDeviceResponseEnum resp = SaveAirDeviceConfigToEEPROM(config) == 0 ?
	CMD_RESP_OK : CMD_RESP_ERR_EN_DIS_JOURNAL;	
	
	txBleBuf[0] = (uint8_t)CMD_RESP_MARKER_JOURNAL;
	txBleBuf[1] = (uint8_t)resp;
	SendBleData(txBleBuf,2);
}

void HandleGetEnDisJournalCommand(AirDeviceConfigTypeDef *config)
{
	uint8_t txBleBuf[2];
	txBleBuf[0] = (uint8_t)CMD_RESP_MARKER_JOURNAL;
	txBleBuf[1] = config->IsJournalEnabled ? 0x01 : 0x00;
	
	SendBleData(txBleBuf,2);
}

#define JEVENT_CHUNK_ITEMS_NUM 10
#define JEVENT_CHUNK_ITEMS_BYTES ((sizeof(JournalEventItemTypeDef)-2) + 2)* JEVENT_CHUNK_ITEMS_NUM
uint8_t chunk_ev[JEVENT_CHUNK_ITEMS_BYTES];
void HandleGetJournalEventsCommand(uint8_t * data, uint32_t len)
{
	if(len != sizeof(uint16_t))
	{
		return;
	}
	
	
	
	uint16_t lastItemsNumReq = *((uint16_t*)data);	
	
	if(lastItemsNumReq > MAX_SAVED_EVENTS_NUM)
	{
		return;
	}	
	
	uint16_t event_items_last_pos; 
	At25Rd((uint8_t*)&event_items_last_pos, AT25256_DATA_COUNTERS_AREA_OFFSET,sizeof(uint16_t));
	
	if(event_items_last_pos > (MAX_SAVED_EVENTS_NUM - 1))
	{
		return;
	}
	
	uint16_t itemsCounter = 0;	
	while(lastItemsNumReq > 0)
	{
		uint16_t chunkSize = lastItemsNumReq > JEVENT_CHUNK_ITEMS_NUM ? JEVENT_CHUNK_ITEMS_NUM : lastItemsNumReq;
	
		for(uint16_t cnt = 0; cnt < chunkSize; cnt++)
		{
		  itemsCounter++;
			
			memcpy(chunk_ev + cnt*((sizeof(JournalEventItemTypeDef)-2) + sizeof(uint16_t)),&itemsCounter,sizeof(uint16_t));
			
			event_items_last_pos = event_items_last_pos > 0 ? (event_items_last_pos - 1) : (MAX_SAVED_EVENTS_NUM - 1);
			
			At25Rd(chunk_ev + (cnt*((sizeof(JournalEventItemTypeDef)-2) + sizeof(uint16_t))) + sizeof(uint16_t), \
				AT25256_EVENT_AREA_OFFSET + (event_items_last_pos * (sizeof(JournalEventItemTypeDef)-2)), (sizeof(JournalEventItemTypeDef)-2));					
		}
		uint16_t sz = chunkSize*((sizeof(JournalEventItemTypeDef)-2) + sizeof(uint16_t));
		
		if(IsConnected)
		{
			SendBleData(chunk_ev,sz);
		}
		else
		{
			
			ResetRxQueues();
			break;
		}
		
		
		lastItemsNumReq -= chunkSize;
	}
	
}

#define JMEAS_CHUNK_ITEMS_NUM 10
#define JMEAS_CHUNK_ITEMS_BYTES (sizeof(JournalMeasureItemTypeDef) + 2) * JMEAS_CHUNK_ITEMS_NUM
uint8_t chunk_m[JMEAS_CHUNK_ITEMS_BYTES];
void HandleGetJournalMeasuresCommand(uint8_t * data, uint32_t len)
{
	if(len != sizeof(uint16_t))
	{
		return;
	}
	
	
	
	uint16_t lastItemsNumReq = *((uint16_t*)data);	
	
	if(lastItemsNumReq > MAX_SAVED_MEASURES_NUM)
	{
		return;
	}	
	
	uint16_t event_items_last_pos; 
	At25Rd((uint8_t*)&event_items_last_pos, AT25256_DATA_COUNTERS_AREA_OFFSET + sizeof(uint16_t),sizeof(uint16_t));
	
	if(event_items_last_pos > (MAX_SAVED_MEASURES_NUM - 1))
	{
		return;
	}
	
	uint16_t itemsCounter = 0;	
	while(lastItemsNumReq > 0)
	{
		uint16_t chunkSize = lastItemsNumReq > JMEAS_CHUNK_ITEMS_NUM ? JMEAS_CHUNK_ITEMS_NUM : lastItemsNumReq;
	
		for(uint16_t cnt = 0; cnt < chunkSize; cnt++)
		{
		  itemsCounter++;
			
			memcpy(chunk_m + cnt*(sizeof(JournalMeasureItemTypeDef) + sizeof(uint16_t)),&itemsCounter,sizeof(uint16_t));
			
			event_items_last_pos = event_items_last_pos > 0 ? (event_items_last_pos - 1) : (MAX_SAVED_MEASURES_NUM - 1);
			
			At25Rd(chunk_m + (cnt*(sizeof(JournalMeasureItemTypeDef) + sizeof(uint16_t))) + sizeof(uint16_t), \
				AT25256_MEASURE_AREA_OFFSET + (event_items_last_pos * sizeof(JournalMeasureItemTypeDef)), sizeof(JournalMeasureItemTypeDef));					
		}
		
		if(IsConnected)
		{
			SendBleData(chunk_m,chunkSize*(sizeof(JournalMeasureItemTypeDef) + sizeof(uint16_t)));
		}
		else
		{
			
			ResetRxQueues();
			break;
		}
		

		
		lastItemsNumReq -= chunkSize;
	}

}