#include "airdevice_common_timers.h"

static bool IsTimeOut = false;

void TimeoutTimerEventHandler(nrf_timer_event_t event_type, void* p_context)
{
    switch (event_type)
    {
        case NRF_TIMER_EVENT_COMPARE0:
						StopTimeoutTimer();
						IsTimeOut = true;						
            break;

        default:
            //Do nothing.
            break;
    }
}

void StartTimeoutTimer(uint32_t secDelay)
{
	 
	 uint32_t time_ticks = 31250*secDelay;

   nrf_drv_timer_extended_compare(
   &timeout_timer, NRF_TIMER_CC_CHANNEL0, time_ticks, NRF_TIMER_SHORT_COMPARE0_CLEAR_MASK, true);
	 IsTimeOut = false;
	 nrf_drv_timer_enable(&timeout_timer);
}

void StopTimeoutTimer(void)
{
	 
  nrf_drv_timer_disable(&timeout_timer);
	nrfx_timer_clear(&timeout_timer);
	IsTimeOut = false;
}

bool CheckTimeout()
{
	if(IsTimeOut)
	{
		IsTimeOut = false;
		return true;
	}
	return IsTimeOut;
}