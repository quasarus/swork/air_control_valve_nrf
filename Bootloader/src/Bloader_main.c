#include "Bloader_main.h"

#define FLASH_CONDITION eepromfwInfo.Version > appfwInfo.Version


static volatile bool isErased = false;
static volatile bool isWrited = false;
static volatile ret_code_t EraseResult = NRF_SUCCESS;
static volatile ret_code_t WriteResult = NRF_SUCCESS;

static void UpdateFirmwareIfExists(void);
static void ProgramApplicationFlash(void);

	
//uint32_t  m_uicr_bootloader_start_address __attribute__((at((uint32_t)&NRF_UICR->NRFFW[0]))) = BOOTLOADER_MEMORY_ADDRESS;

int main(void)
{	
	nrfx_irq_handler_t reset_handler = (nrfx_irq_handler_t)((volatile uint32_t const *)APP_MEMORY_ADDRESS)[1];

	sd_softdevice_disable();
	
	UpdateFirmwareIfExists();

	sd_softdevice_vector_table_base_set(APP_MEMORY_ADDRESS);
	__disable_irq();

  reset_handler();
	while(1)
	{
		
	}
}

static void UpdateFirmwareIfExists(void)
{
	FirmwareInfoTypeDef eepromfwInfo;
	FirmwareInfoTypeDef appfwInfo;
	
	GetEepromFirmwareInfo(&eepromfwInfo);
	GetApplicationFirmwareInfo(&appfwInfo);
	
	if(IsFirmwareExists(&eepromfwInfo) && IsFirmwareExists(&appfwInfo))
	{
		if(FLASH_CONDITION)
		{
			ProgramApplicationFlash();
		}		
	}
	else if(IsFirmwareExists(&eepromfwInfo) && appfwInfo.Marker1 == 0xFFFFFFFF &&
		appfwInfo.Marker2 == 0xFFFFFFFF && appfwInfo.Marker3 == 0xFFFFFFFF && appfwInfo.Version == 0xFFFFFFFF)
	{
		ProgramApplicationFlash();
	}
}

void ProgramApplicationFlash(void)
{
	uint32_t fwSize = *((uint32_t*)(FW_MEMORY_ADDRESS + sizeof(uint32_t)));
	uint32_t fwCrc = *((uint32_t*)(FW_MEMORY_ADDRESS));
	uint8_t *pfwData = (uint8_t*)(FW_MEMORY_ADDRESS + sizeof(uint32_t)*2);
	
	uint32_t calcFwCRC = CalculateCRC32(pfwData,fwSize);
#ifndef DEBUG
	if(calcFwCRC != fwCrc)
	{
		return;
	}
#endif
	if(fwSize > FW_MEMORY_SIZE)
	{
		return;
	}
	
	uint16_t pagesToEraseNum = fwSize/NRF52_FLASH_PAGE_SIZE;
	if(fwSize % NRF52_FLASH_PAGE_SIZE > 0)
	{
		pagesToEraseNum++;
	}
	
	if(pagesToEraseNum > FW_MEMORY_MAX_PAGES_NUM)
	{
		return;
	}

	for(uint16_t cnt=0;cnt < pagesToEraseNum;cnt++)
	{
		nrf_nvmc_page_erase(APP_MEMORY_ADDRESS + (cnt * NRF52_FLASH_PAGE_SIZE));

	}
	
	nrf_delay_ms(500);
	
	uint32_t fullWordsLen = (fwSize / sizeof(uint32_t));
  uint32_t stayLenBytes = fwSize % sizeof(uint32_t);
	
	if(fullWordsLen > 0)
	{
		nrf_nvmc_write_words(APP_MEMORY_ADDRESS,(uint32_t*)pfwData,fullWordsLen);
	}
	if(stayLenBytes > 0)
	{
		nrf_nvmc_write_bytes(APP_MEMORY_ADDRESS + fullWordsLen*sizeof(uint32_t),pfwData,stayLenBytes);
	}
	
	uint32_t calcCRC = CalculateCRC32((uint8_t*)APP_MEMORY_ADDRESS,fwSize);	
	
	if(calcCRC == fwCrc)
	{	
			//	
	}
	
	NVIC_SystemReset();
	
}


