#ifndef __B_LOADER_MAIN__
#define __B_LOADER_MAIN__

#include "nrf52.h"

//#define BOOTLOADER_START_ADDR  

#if defined(SOFTDEVICE_PRESENT)
#include "nrf_fstorage_sd.h"
#else
#include "nrf_fstorage_nvmc.h"
#endif
//#include "airdevice_eeprom.h"
#include "public.h"
#include "nrf_sdm.h"
#include <stddef.h>
#include "nrf_fstorage.h"
#include "nrfx_common.h"
#include "nrf_nvmc.h"
#include "nrf_nvic.h"
#include "nrf_delay.h"
#include "crc32_calc.h"
#endif

	

