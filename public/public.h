#ifndef __PUBLIC_H
#define __PUBLIC_H

#define IS_WRITE_FW_TO_EXTERNAL_FLASH 0
#define IS_WRITE_FW_TO_INTERNAL_FLASH 1

#include <stdint.h>
#include <stdbool.h>

#define FIRMWARE_INFO_OFFSET 0xC000

#define FW_INFO_MARKER_1 0x34AD56E0
#define FW_INFO_MARKER_2 0x35AD56E1
#define FW_INFO_MARKER_3 0x34AD57E2

typedef struct
{
	uint32_t Marker1;
	uint32_t Marker2;
	uint32_t Marker3;
	uint32_t Version;
}FirmwareInfoTypeDef;

typedef enum
{
	CMD_SET_DAMPER_UNKNOWN,
	CMD_SET_DAMPER_POS,	
	CMD_WRITE_TO_EEPROM,
	CMD_END_WRITE_TO_EEPROM,
	CMD_PREPARE_TO_WRITE_TO_EEPROM,
	CMD_GET_TEMPERATURE_AND_HUMIDITY,
	CMD_GET_FW_VERSION,
	CMD_SET_DEVICE_NAME,
	CMD_SET_WAKEUP_INTERVAL_SET,
	CMD_SET_WAKEUP_INTERVAL_GET,
	CMD_DUMPER_CALIBRATE,
	CMD_DATETIME_SET,
	CMD_DATETIME_GET,
	CMD_SET_EN_DIS_JOURNAL,
	CMD_GET_EN_DIS_JOURNAL,
	CMD_GET_JOURNAL_EVENTS,
	CMD_GET_JOURNAL_MEASURES
}AirDeviceCommandTypeDef;

typedef enum 
{
	CMD_RESP_MARKER_DAMPER = 0x33,
	CMD_RESP_MARKER_FILE_OP = 0x34,
	CMD_RESP_MARKER_TEMPERATURE_AND_HUMIDITY = 0x35,
	CMD_RESP_MARKER_FW_VERSION = 0x36,
	CMD_RESP_MARKER_DEVICE_NAME_SET = 0x37,
	CMD_RESP_MARKER_WAKEUP_INTERVAL = 0x38,
	CMD_RESP_MARKER_CALIBRATION = 0x39,
	CMD_RESP_MARKER_DATETIME = 0x40,
	CMD_RESP_MARKER_JOURNAL = 0x41
}AirDeviceResponseMarkerEnum;

typedef enum
{
	CMD_RESP_OK = 0,	
	CMD_RESP_ERR_FILE_WRONG_LEN,
	CMD_RESP_ERR_FILE_WRONG_CRC,	
	CMD_RESP_GET_TEMP_HUM_ERROR,
	CMD_RESP_ERR_MALLOC,
	CMD_RESP_ERR_FILE_ERASE_ERROR,
	CMD_RESP_ERR_FILE_WRITE_ERROR,
	CMD_RESP_ERR_DEV_NAME_WRONG_SIZE,
	CMD_RESP_ERR_DEV_NAME_SAVE_ERROR,
	CMD_RESP_ERR_WAKEUP_INTERVAL_WRONG_VAL_ERROR,
	CMD_RESP_ERR_WAKEUP_INTERVAL_SAVE_ERROR,
	CMD_RESP_ERR_CALIBRATION,
	CMD_RESP_ERR_DUMPER_SET,
	CMD_RESP_ERR_EN_DIS_JOURNAL
}AirDeviceResponseEnum;


#define NRF52_FLASH_PAGE_SIZE 0x1000

#define BOOTLOADER_MEMORY_ADDRESS 0x26000
#define BOOTLOADER_MEMORY_SIZE 0x8000

#define APP_MEMORY_ADDRESS (BOOTLOADER_MEMORY_ADDRESS + BOOTLOADER_MEMORY_SIZE)
#define APP_MEMORY_SIZE 0x29000
#define FW_MEMORY_ADDRESS (APP_MEMORY_ADDRESS + APP_MEMORY_SIZE)	//0x4F000
#define FW_MEMORY_SIZE APP_MEMORY_SIZE


#define FW_MEMORY_MAX_PAGES_NUM (FW_MEMORY_SIZE/NRF52_FLASH_PAGE_SIZE)

uint8_t CheckEepromFirmwareIntegrity(void);
bool IsFirmwareExists(FirmwareInfoTypeDef *fwInfo);
bool IsDataErased(uint8_t *data, uint32_t len);
void GetEepromFirmwareInfo(FirmwareInfoTypeDef *fwInfo);
void GetApplicationFirmwareInfo(FirmwareInfoTypeDef *fwInfo);

void ControlLed(bool isEnabled);
void ControlEncoderPower(bool isEnabled);
void Control5VoltPower(bool isEnabled);
void Control5VoltDcDc(bool isEnabled);
void ControlBatteryAdcDivider(bool isEnabled);
void ControlTemperatureSensor(bool isEnabled);
#endif
