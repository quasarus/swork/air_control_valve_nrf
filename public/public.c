#include "public.h"
#include "crc32_calc.h"


uint8_t CheckEepromFirmwareIntegrity()
{
	
#if IS_WRITE_FW_TO_EXTERNAL_FLASH == 1 && IS_WRITE_FW_TO_INTERNAL_FLASH == 0
	
	At25RdPage((uint8_t*)fw_crc_and_len,SECTOR_SIZE,0,sizeof(uint32_t)*2);

	uint32_t fw_crc_and_len[2];
	uint32_t fw_crc = fw_crc_and_len[0];
	uint32_t fw_len = fw_crc_and_len[1];
	uint32_t bin_len = fw_len + sizeof(uint32_t)*2;
	uint32_t offset = 0;
	uint32_t address = SECTOR_SIZE;
	if(fw_len > ((w25qxx.SectorCount*w25qxx.SectorSize) - SECTOR_SIZE))
	{
		return CMD_RESP_ERR_FILE_WRONG_CRC;
	}
	uint8_t *pTemp = malloc(W25QXX_PAGE_SIZE);
	if(pTemp == NULL)
	{
		return CMD_RESP_ERR_MALLOC;
	}
	uint32_t crc = ~0U;	
	
	while(offset < bin_len)
	{
		
		uint16_t chunk = (bin_len - offset) < W25QXX_PAGE_SIZE ? (bin_len - offset) : W25QXX_PAGE_SIZE;		
		

		At25RdPage(pTemp,SECTOR_SIZE + offset,0,W25QXX_PAGE_SIZE);
		
		uint32_t addOffset = offset == 0 ? sizeof(uint32_t)*2 : 0;
		
		for(uint16_t cnt = 0; cnt < (chunk - addOffset); cnt++)
		{
			crc = crc32_tab[(crc ^ pTemp[cnt + addOffset]) & 0xFF] ^ (crc >> 8);
		}	
		
		offset += chunk;

	}
	free(pTemp);
	crc = crc ^ ~0U;
	
	return crc == fw_crc ? (uint8_t)CMD_RESP_OK : (uint8_t)CMD_RESP_ERR_FILE_WRONG_CRC;
#elif IS_WRITE_FW_TO_EXTERNAL_FLASH == 0 && IS_WRITE_FW_TO_INTERNAL_FLASH == 1	
	uint32_t *pfw_crc_and_len = (uint32_t*)FW_MEMORY_ADDRESS;
	uint32_t fw_crc = pfw_crc_and_len[0];
	uint32_t fw_len = pfw_crc_and_len[1];
	uint32_t calcCrc = CalculateCRC32(pfw_crc_and_len + 2,fw_len);
	return calcCrc == fw_crc ? (uint8_t)CMD_RESP_OK : (uint8_t)CMD_RESP_ERR_FILE_WRONG_CRC;
#else
#error "Select target firmware flash."
#endif	
	  
}

bool IsFirmwareExists(FirmwareInfoTypeDef *fwInfo)
{
	
	return fwInfo->Marker1 == FW_INFO_MARKER_1 && fwInfo->Marker2 == FW_INFO_MARKER_2 && fwInfo->Marker3 == FW_INFO_MARKER_3;	
}

bool IsDataErased(uint8_t *data, uint32_t len)
{
	uint32_t ffsCnt = 0;
	for(uint32_t cnt = 0; cnt < len; cnt++)
	{
		if(*(data + cnt) == 0xFF)
		{
			ffsCnt++;
		}
	}
	return ffsCnt == len ? true : false;
}

void GetEepromFirmwareInfo(FirmwareInfoTypeDef *fwInfo)
{	
#if IS_WRITE_FW_TO_EXTERNAL_FLASH == 1 && IS_WRITE_FW_TO_INTERNAL_FLASH == 0
	At25RdPage((uint8_t*)fwInfo,W25QXX_SECTOR_SIZE + FIRMWARE_INFO_OFFSET + sizeof(uint32_t)*2,0,sizeof(FirmwareInfoTypeDef));
#elif IS_WRITE_FW_TO_EXTERNAL_FLASH == 0 && IS_WRITE_FW_TO_INTERNAL_FLASH == 1	
	memcpy(fwInfo,((uint8_t*)(FW_MEMORY_ADDRESS + FIRMWARE_INFO_OFFSET + sizeof(uint32_t)*2)),sizeof(FirmwareInfoTypeDef));

#else
#error "Select target firmware flash."
#endif
}

void GetApplicationFirmwareInfo(FirmwareInfoTypeDef *fwInfo)
{	
	memcpy(fwInfo,((uint8_t*)(APP_MEMORY_ADDRESS + FIRMWARE_INFO_OFFSET)),sizeof(FirmwareInfoTypeDef));
}

void ControlLed(bool isEnabled)
{
	if(isEnabled)
	{
		nrf_gpio_cfg_output(LED_IND_PIN);
		nrf_gpio_pin_write(LED_IND_PIN,1);
	}
	else
	{
		nrf_gpio_pin_write(LED_IND_PIN,0);
		nrf_gpio_cfg_default(LED_IND_PIN);
	}
		
}

void ControlEncoderPower(bool isEnabled)	
{
	if(isEnabled)
	{
		nrf_gpio_cfg_output(ENC_POWER_EN_PIN);
		nrf_gpio_pin_write(ENC_POWER_EN_PIN,0);
	}
	else
	{
		nrf_gpio_pin_write(ENC_POWER_EN_PIN,1);
		nrf_gpio_cfg_default(ENC_POWER_EN_PIN);
	}
	
}

void Control5VoltPower(bool isEnabled)
{
	if(isEnabled)
	{
		//nrf_gpio_cfg_output(EN_5V_PIN);
		nrf_gpio_pin_write(EN_5V_PIN,0);
	}
	else
	{
		nrf_gpio_pin_write(EN_5V_PIN,1);
		//nrf_gpio_cfg_default(EN_5V_PIN);
	}
}

void Control5VoltDcDc(bool isEnabled)
{
	if(isEnabled)
	{		
		//nrf_gpio_cfg_output(DC5V_EN_PIN);
		nrf_gpio_pin_write(DC5V_EN_PIN,1);
	}
	else
	{
		nrf_gpio_pin_write(DC5V_EN_PIN,0);
		//nrf_gpio_cfg_default(DC5V_EN_PIN);
	}
	
}

void ControlTemperatureSensor(bool isEnabled)
{
	if(isEnabled)
	{
		nrf_gpio_cfg_output(VCC_T_EN_PIN);
		nrf_gpio_pin_write(VCC_T_EN_PIN,0);
	}
	else
	{
		nrf_gpio_pin_write(VCC_T_EN_PIN,1);
		nrf_gpio_cfg_default(VCC_T_EN_PIN);
	}
}

void ControlBatteryAdcDivider(bool isEnabled)
{
	if(isEnabled)
	{
		nrf_gpio_cfg_output(ADC_EN_PIN);
		nrf_gpio_pin_write(ADC_EN_PIN,1);
	}
	else
	{
		nrf_gpio_pin_write(ADC_EN_PIN,0);
		nrf_gpio_cfg_default(ADC_EN_PIN);
	}	
}
