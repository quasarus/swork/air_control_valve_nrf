//APP_TIMER_V2 APP_TIMER_V2_RTC1_ENABLED BOARD_PCA10040
/**
 * Copyright (c) 2014 - 2019, Nordic Semiconductor ASA
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form, except as embedded into a Nordic
 *    Semiconductor ASA integrated circuit in a product or a software update for
 *    such product, must reproduce the above copyright notice, this list of
 *    conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 *
 * 3. Neither the name of Nordic Semiconductor ASA nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * 4. This software, with or without modification, must only be used with a
 *    Nordic Semiconductor ASA integrated circuit.
 *
 * 5. Any software provided in binary form under this license must not be reverse
 *    engineered, decompiled, modified and/or disassembled.
 *
 * THIS SOFTWARE IS PROVIDED BY NORDIC SEMICONDUCTOR ASA "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NORDIC SEMICONDUCTOR ASA OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
/** @file
 *
 * @defgroup ble_sdk_uart_over_ble_main main.c
 * @{
 * @ingroup  ble_sdk_app_airdevice_eval
 * @brief    UART over BLE application main file.
 *
 * This file contains the source code for a sample application that uses the Nordic UART service.
 * This application uses the @ref srvlib_conn_params module.
 */


#include "main.h"
#include "version.h"

//uint32_t  m_uicr_bootloader_start_address __attribute__((at((uint32_t)&NRF_UICR->NRFFW[0]))) = APP_MEMORY_ADDRESS;



#define AIRDEVICE_SERVICE_UUID_TYPE           BLE_UUID_TYPE_VENDOR_BEGIN                  /**< UUID type for the Nordic UART Service (vendor specific). BLE_UUID_TYPE_VENDOR_BEGIN  */

#define APP_BLE_OBSERVER_PRIO           3                                           /**< Application's BLE observer priority. You shouldn't need to modify this value. */





#define MIN_CONN_INTERVAL               MSEC_TO_UNITS(7.5, UNIT_1_25_MS)   //12          															/**< Minimum acceptable connection interval (20 ms), Connection interval uses 1.25 ms units. */
#define MAX_CONN_INTERVAL               MSEC_TO_UNITS(200, UNIT_1_25_MS)             															/**< Maximum acceptable connection interval (75 ms), Connection interval uses 1.25 ms units. */
#define SLAVE_LATENCY                   0                                           /**< Slave latency. */
#define CONN_SUP_TIMEOUT                MSEC_TO_UNITS(4000, UNIT_10_MS)             															/**< Connection supervisory timeout (4 seconds), Supervision Timeout uses 10 ms units. */
#define FIRST_CONN_PARAMS_UPDATE_DELAY  APP_TIMER_TICKS(5000)                       /**< Time from initiating event (connect or start of notification) to first time sd_ble_gap_conn_param_update is called (5 seconds). */
#define NEXT_CONN_PARAMS_UPDATE_DELAY   APP_TIMER_TICKS(30000)                      /**< Time between each call to sd_ble_gap_conn_param_update after the first call (30 seconds). */
#define MAX_CONN_PARAMS_UPDATE_COUNT    3                                           /**< Number of attempts before giving up the connection parameter negotiation. */

#define DEAD_BEEF                       0xDEADBEEF                                  /**< Value used as error code on stack dump, can be used to identify stack location on stack unwind. */

#define UART_TX_BUF_SIZE                256                                         /**< UART TX buffer size. */
#define UART_RX_BUF_SIZE                256                                         /**< UART RX buffer size. */

#define SCAN_DURATION_WITELIST      3000                                /**< Duration of the scanning in units of 10 milliseconds. */
//#define AIRDEVICE_COMPANY_ID 0x000D

BLE_AIRDEVICE_DEF(m_airdevice, NRF_SDH_BLE_TOTAL_LINK_COUNT);                                   /**< BLE airdevice service instance. */
NRF_BLE_GATT_DEF(m_gatt);                                                           /**< GATT module instance. */
NRF_BLE_QWR_DEF(m_qwr);                                                             /**< Context for the Queued Write module.*/

NRF_BLE_SCAN_DEF(m_scan);                                           /**< Scanning module instance. */

BleTransactDataTypeDef BleTransactData =
{
	.pAirDevInstance = &m_airdevice
};

static uint16_t   m_conn_handle          = BLE_CONN_HANDLE_INVALID;                 /**< Handle of the current connection. */
static uint16_t   m_ble_airdevice_max_data_len = BLE_GATT_ATT_MTU_DEFAULT - 3;            /**< Maximum length of data (in bytes) that can be transmitted to the peer by the Nordic UART service module. */
static void sleep_mode_enter(void);


static bool     m_memory_access_in_progress;                        /**< Flag to keep track of ongoing operations on persistent memory. */

volatile bool IsConnected = false;

uint16_t GetConnectionHandle()
{
	return m_conn_handle;
}

uint16_t GetMaxDataLen(void)
{
	return m_ble_airdevice_max_data_len;
}



/**@brief Function for assert macro callback.
 *
 * @details This function will be called in case of an assert in the SoftDevice.
 *
 * @warning This handler is an example only and does not fit a final product. You need to analyse
 *          how your product is supposed to react in case of Assert.
 * @warning On assert from the SoftDevice, the system can only recover on reset.
 *
 * @param[in] line_num    Line number of the failing ASSERT call.
 * @param[in] p_file_name File name of the failing ASSERT call.
 */
void assert_nrf_callback(uint16_t line_num, const uint8_t * p_file_name)
{
    app_error_handler(DEAD_BEEF, line_num, p_file_name);
}

/**@brief Function for initializing the timer module.
 */
static void timers_init(void)
{
    ret_code_t err_code = app_timer_init();
    APP_ERROR_CHECK(err_code);
}

/**@brief Function for the GAP initialization.
 *
 * @details This function will set up all the necessary GAP (Generic Access Profile) parameters of
 *          the device. It also sets the permissions and appearance.
 */
static void gap_params_init(void)
{
    uint32_t                err_code;
    ble_gap_conn_params_t   gap_conn_params;
    ble_gap_conn_sec_mode_t sec_mode;

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&sec_mode);

//    err_code = sd_ble_gap_device_name_set(&sec_mode,
//                                          (const uint8_t *) DEVICE_NAME,
//                                          strlen(DEVICE_NAME));
//    APP_ERROR_CHECK(err_code);

    memset(&gap_conn_params, 0, sizeof(gap_conn_params));

    gap_conn_params.min_conn_interval = MIN_CONN_INTERVAL;
    gap_conn_params.max_conn_interval = MAX_CONN_INTERVAL;
    gap_conn_params.slave_latency     = SLAVE_LATENCY;
    gap_conn_params.conn_sup_timeout  = CONN_SUP_TIMEOUT;

    err_code = sd_ble_gap_ppcp_set(&gap_conn_params);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for handling Queued Write Module errors.
 *
 * @details A pointer to this function will be passed to each service which may need to inform the
 *          application about an error.
 *
 * @param[in]   nrf_error   Error code containing information about what went wrong.
 */
static void nrf_qwr_error_handler(uint32_t nrf_error)
{
    APP_ERROR_HANDLER(nrf_error);
}


static void airdevice_data_handler(ble_airdevice_evt_t * p_evt)
{
	switch(p_evt->type)
	{
		case BLE_AIRDEVICE_EVT_DATA_RX:																										//�� ������ ������ �� BLE, �������� ��� � �������
		{
			PutDataToRxQueue(p_evt->params.rx_data.p_data, p_evt->params.rx_data.length);

			break;
		}
		case BLE_AIRDEVICE_EVT_TX_RDY:
		{
			//NRF_LOG_INFO("Txed");
			break;
		}
		case BLE_AIRDEVICE_EVT_COMM_STARTED:
		{
			break;
		}
		case BLE_AIRDEVICE_EVT_COMM_STOPPED:
		{
			break;
		}
		default:
			break;
	}

}
/**@snippet [Handling the data received over BLE] */


/**@brief Function for initializing services that will be used by the application.
 */
static void services_init(void)
{
    uint32_t           err_code;
    ble_airdevice_init_t     airdevice_init;
    nrf_ble_qwr_init_t qwr_init = {0};

    // Initialize Queued Write Module.
    qwr_init.error_handler = nrf_qwr_error_handler;

    err_code = nrf_ble_qwr_init(&m_qwr, &qwr_init);
    APP_ERROR_CHECK(err_code);

    // Initialize airdevice.
    memset(&airdevice_init, 0, sizeof(airdevice_init));

    airdevice_init.data_handler = airdevice_data_handler;
		
    err_code = ble_airdevice_init(&m_airdevice, &airdevice_init);
    APP_ERROR_CHECK(err_code);
		

}


/**@brief Function for handling an event from the Connection Parameters Module.
 *
 * @details This function will be called for all events in the Connection Parameters Module
 *          which are passed to the application.
 *
 * @note All this function does is to disconnect. This could have been done by simply setting
 *       the disconnect_on_fail config parameter, but instead we use the event handler
 *       mechanism to demonstrate its use.
 *
 * @param[in] p_evt  Event received from the Connection Parameters Module.
 */
static void on_conn_params_evt(ble_conn_params_evt_t * p_evt)
{
    uint32_t err_code;

    if (p_evt->evt_type == BLE_CONN_PARAMS_EVT_FAILED)
    {
        err_code = sd_ble_gap_disconnect(m_conn_handle, BLE_HCI_CONN_INTERVAL_UNACCEPTABLE);
        APP_ERROR_CHECK(err_code);
    }
}


/**@brief Function for handling errors from the Connection Parameters module.
 *
 * @param[in] nrf_error  Error code containing information about what went wrong.
 */
static void conn_params_error_handler(uint32_t nrf_error)
{
    APP_ERROR_HANDLER(nrf_error);
}


/**@brief Function for initializing the Connection Parameters module.
 */
static void conn_params_init(void)
{
    uint32_t               err_code;
    ble_conn_params_init_t cp_init;

    memset(&cp_init, 0, sizeof(cp_init));

    cp_init.p_conn_params                  = NULL;
    cp_init.first_conn_params_update_delay = FIRST_CONN_PARAMS_UPDATE_DELAY;
    cp_init.next_conn_params_update_delay  = NEXT_CONN_PARAMS_UPDATE_DELAY;
    cp_init.max_conn_params_update_count   = MAX_CONN_PARAMS_UPDATE_COUNT;
    cp_init.start_on_notify_cccd_handle    = BLE_GATT_HANDLE_INVALID;
    cp_init.disconnect_on_fail             = false;
    cp_init.evt_handler                    = on_conn_params_evt;
    cp_init.error_handler                  = conn_params_error_handler;

    err_code = ble_conn_params_init(&cp_init);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for putting the chip into sleep mode.
 *
 * @note This function will not return.
 */
//static void sleep_mode_enter(void)
//{
//    uint32_t err_code = bsp_indication_set(BSP_INDICATE_IDLE);
//    APP_ERROR_CHECK(err_code);

//    // Prepare wakeup buttons.
//    err_code = bsp_btn_ble_sleep_mode_prepare();
//    APP_ERROR_CHECK(err_code);

//    // Go to system-off mode (this function will not return; wakeup will cause a reset).
//    err_code = sd_power_system_off();
//    APP_ERROR_CHECK(err_code);
//}



static void sleep_mode_enter(void)
{
    ret_code_t err_code;

    //err_code = bsp_indication_set(BSP_INDICATE_IDLE);
    //APP_ERROR_CHECK(err_code);

    // Prepare wakeup buttons.
    //err_code = bsp_btn_ble_sleep_mode_prepare();
    //APP_ERROR_CHECK(err_code);

    // Go to system-off mode (this function will not return; wakeup will cause a reset).
    //err_code = sd_power_system_off();
    APP_ERROR_CHECK(err_code);
}

/**@brief Function for handling BLE events.
 *
 * @param[in]   p_ble_evt   Bluetooth stack event.
 * @param[in]   p_context   Uairdeviceed.
 */
static void ble_evt_handler(ble_evt_t const * p_ble_evt, void * p_context)
{
    uint32_t err_code;
		ble_gap_evt_t const * p_gap_evt = &p_ble_evt->evt.gap_evt;
		//p_ble_evt->gap_evt.params.adv_report;
    switch (p_ble_evt->header.evt_id)
    {
        case BLE_GAP_EVT_ADV_REPORT :
				{
					ble_gap_evt_adv_report_t report = p_gap_evt->params.adv_report;
					if(report.data.len == BLE_ADV_BASE_DATA_LEN)
					{
						if(report.data.p_data[1] == BLE_GAP_AD_TYPE_16BIT_SERVICE_UUID_COMPLETE &&
							report.data.p_data[2] == BLE_ADV_ID_BYTE1 && report.data.p_data[3] == BLE_ADV_ID_BYTE2 &&
							 report.data.p_data[5] == BLE_ADV_ID_BYTE_BASE_DEVICE)
						{
							SetBaseDeviceVisibility(true);
							UpdateAdvData();
						}					
						
					}
					break;
				}				
        case BLE_GAP_EVT_CONNECTED:
            NRF_LOG_INFO("Connected");
						//nrf_ble_scan_stop();
						APP_ERROR_CHECK(err_code);
            //err_code = bsp_indication_set(BSP_INDICATE_CONNECTED);
            //APP_ERROR_CHECK(err_code);
						OnDeviceConnect();
						IsConnected = true;
            m_conn_handle = p_ble_evt->evt.gap_evt.conn_handle;
            err_code = nrf_ble_qwr_conn_handle_assign(&m_qwr, m_conn_handle);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_GAP_EVT_DISCONNECTED:
            NRF_LOG_INFO("Disconnected, reason %d.",
                          p_ble_evt->evt.gap_evt.params.disconnected.reason);
						//NRF_LOG_FLUSH();
						//err_code = nrf_ble_scan_start(&m_scan);
						APP_ERROR_CHECK(err_code);
						OnDeviceDisconnect();
            IsConnected = false;
            m_conn_handle = BLE_CONN_HANDLE_INVALID;
            break;

        case BLE_GAP_EVT_PHY_UPDATE_REQUEST:
        {
            NRF_LOG_DEBUG("PHY update request.");
            ble_gap_phys_t const phys =
            {
                .rx_phys = BLE_GAP_PHY_AUTO,
                .tx_phys = BLE_GAP_PHY_AUTO,
            };
            err_code = sd_ble_gap_phy_update(p_ble_evt->evt.gap_evt.conn_handle, &phys);
            APP_ERROR_CHECK(err_code);
        } break;

        case BLE_GAP_EVT_SEC_PARAMS_REQUEST:
            // Pairing not supported
            err_code = sd_ble_gap_sec_params_reply(m_conn_handle, BLE_GAP_SEC_STATUS_PAIRING_NOT_SUPP, NULL, NULL);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_GATTS_EVT_SYS_ATTR_MISSING:
            // No system attributes have been stored.
            err_code = sd_ble_gatts_sys_attr_set(m_conn_handle, NULL, 0, 0);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_GATTC_EVT_TIMEOUT:
            // Disconnect on GATT Client timeout event.
            err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gattc_evt.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_GATTS_EVT_TIMEOUT:
            // Disconnect on GATT Server timeout event.
            err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gatts_evt.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            APP_ERROR_CHECK(err_code);
            break;

        default:
            // No implementation needed.
            break;
    }
}


/**@brief Function for the SoftDevice initialization.
 *
 * @details This function initializes the SoftDevice and the BLE event interrupt.
 */
static void ble_stack_init(void)
{
    ret_code_t err_code;

    err_code = nrf_sdh_enable_request();
    APP_ERROR_CHECK(err_code);

    // Configure the BLE stack using the default settings.
    // Fetch the start address of the application RAM.
    uint32_t ram_start = 0;
    err_code = nrf_sdh_ble_default_cfg_set(APP_BLE_CONN_CFG_TAG, &ram_start);
    APP_ERROR_CHECK(err_code);
	
//	  ble_cfg_t ble_cfg;
//    memset(&ble_cfg, 0, sizeof ble_cfg);
//    ble_cfg.conn_cfg.conn_cfg_tag = APP_BLE_CONN_CFG_TAG;
//    ble_cfg.conn_cfg.params.gatts_conn_cfg.hvn_tx_queue_size = 8;
//    err_code = sd_ble_cfg_set(BLE_CONN_CFG_GATTS, &ble_cfg, ram_start);
//    APP_ERROR_CHECK(err_code);


    // Enable BLE stack.
    err_code = nrf_sdh_ble_enable(&ram_start);
    APP_ERROR_CHECK(err_code);

    // Register a handler for BLE events.
    NRF_SDH_BLE_OBSERVER(m_ble_observer, APP_BLE_OBSERVER_PRIO, ble_evt_handler, NULL);
}


/**@brief Function for handling events from the GATT library. */
void gatt_evt_handler(nrf_ble_gatt_t * p_gatt, nrf_ble_gatt_evt_t const * p_evt)
{
    if ((m_conn_handle == p_evt->conn_handle) && (p_evt->evt_id == NRF_BLE_GATT_EVT_ATT_MTU_UPDATED))
    {
        m_ble_airdevice_max_data_len = p_evt->params.att_mtu_effective - OPCODE_LENGTH - HANDLE_LENGTH;
        NRF_LOG_INFO("Data len is set to 0x%X(%d)", m_ble_airdevice_max_data_len, m_ble_airdevice_max_data_len);
    }
    NRF_LOG_DEBUG("ATT MTU exchange completed. central 0x%x peripheral 0x%x",
                  p_gatt->att_mtu_desired_central,
                  p_gatt->att_mtu_desired_periph);
}


/**@brief Function for initializing the GATT library. */
void gatt_init(void)
{
    ret_code_t err_code;

    err_code = nrf_ble_gatt_init(&m_gatt, gatt_evt_handler);
    APP_ERROR_CHECK(err_code);

    err_code = nrf_ble_gatt_att_mtu_periph_set(&m_gatt, NRF_SDH_BLE_GATT_MAX_MTU_SIZE);
    APP_ERROR_CHECK(err_code);
}


//typedef struct {
//	uint8_t len;
//	uint8_t type;
//	uint16_t val;
//}manuf_specific_data_t;

//const manuf_specific_data_t manuf_specific_data =
//{
//	.len = 3,
//	.type = BLE_GAP_AD_TYPE_MANUFACTURER_SPECIFIC_DATA,
//	.val = 0x0D00
//};

static void advertising_init(void)
{	
	AdvertisingInit(&AirDeviceConfig);
}




/**@brief Function for initializing the nrf log module.
 */
static void log_init(void)
{
    ret_code_t err_code = NRF_LOG_INIT(NULL);
    APP_ERROR_CHECK(err_code);

    NRF_LOG_DEFAULT_BACKENDS_INIT();
}


/**@brief Function for initializing power management.
 */
static void power_management_init(void)
{
    ret_code_t err_code;
    err_code = nrf_pwr_mgmt_init();
    APP_ERROR_CHECK(err_code);
}


//////////��������, �����������
//static void idle_state_handle(void)
//{
//    if (NRF_LOG_PROCESS() == false)
//    {
//        nrf_pwr_mgmt_run();
//    }
//}


/**@brief Function for starting advertising.
 */
static void advertising_start(void)
{
   AdvertiseStart();
}


static ble_gap_scan_params_t const m_scan_param =
{		
    .active        = 0x01,
    .interval      = NRF_BLE_SCAN_SCAN_INTERVAL,
    .window        = NRF_BLE_SCAN_SCAN_WINDOW,
    .filter_policy = BLE_GAP_SCAN_FP_ACCEPT_ALL,
    .timeout       = SCAN_DURATION_WITELIST,
    .scan_phys     = BLE_GAP_PHY_1MBPS,
};



static void scan_evt_handler(scan_evt_t const * p_scan_evt)
{
    ret_code_t err_code;
    switch(p_scan_evt->scan_evt_id)
    {
        case NRF_BLE_SCAN_EVT_WHITELIST_REQUEST:
        {
            
            //m_whitelist_disabled = false;
        } break;

        case NRF_BLE_SCAN_EVT_CONNECTING_ERROR:
        {
            err_code = p_scan_evt->params.connecting_err.err_code;
            APP_ERROR_CHECK(err_code);
        } break;

        case NRF_BLE_SCAN_EVT_SCAN_TIMEOUT:
        {
            NRF_LOG_INFO("Scan timed out.");
            //scan_start();
        } break;

        case NRF_BLE_SCAN_EVT_FILTER_MATCH:
						
            break;
        case NRF_BLE_SCAN_EVT_WHITELIST_ADV_REPORT:
						
            break;

        default:
          break;
    }
}

/**@brief Function for initialization scanning and setting filters.
 */
static void scan_init(void)
{
    ret_code_t          err_code;
    nrf_ble_scan_init_t init_scan;

    memset(&init_scan, 0, sizeof(init_scan));

    init_scan.p_scan_param     = &m_scan_param;
    init_scan.connect_if_match = false;
    init_scan.conn_cfg_tag     = APP_BLE_CONN_CFG_TAG;

    err_code = nrf_ble_scan_init(&m_scan, &init_scan, scan_evt_handler);
    APP_ERROR_CHECK(err_code);


}

/** @brief Clear bonding information from persistent storage
 */
static void delete_bonds(void)
{
    ret_code_t err_code;

    NRF_LOG_INFO("Erase bonds!");
	
    err_code = pm_peers_delete();
    APP_ERROR_CHECK(err_code);
}

static void scan_start(void)
{
    ret_code_t err_code;

    if (nrf_fstorage_is_busy(NULL))
    {
        m_memory_access_in_progress = true;
        return;
    }

    NRF_LOG_INFO("Starting scan.");

    err_code = nrf_ble_scan_start(&m_scan);
    APP_ERROR_CHECK(err_code);

}
/**@brief Function for starting a scan, or instead trigger it from peer manager (after
 *        deleting bonds).
 *
 * @param[in] p_erase_bonds Pointer to a bool to determine if bonds will be deleted before scanning.
 */
void scanning_start(bool * p_erase_bonds)
{
    // Start scanning for peripherals and initiate connection
    // with devices that advertise GATT Service UUID.
    if (*p_erase_bonds == true)
    {
        // Scan is started by the PM_EVT_PEERS_DELETE_SUCCEEDED event.
        delete_bonds();
    }
    else
    {
        scan_start();
    }
}

/**
 * @brief Database discovery collector initialization.
 */
//static void db_discovery_init(void)
//{
//    ble_db_discovery_init_t db_init;

//    memset(&db_init, 0, sizeof(db_init));

//    db_init.evt_handler  = db_disc_handler;
//    db_init.p_gatt_queue = &m_ble_gatt_queue;

//    ret_code_t err_code = ble_db_discovery_init(&db_init);

//    APP_ERROR_CHECK(err_code);
//}


//static uint32_t m_systick_cnt;

//void SysTick_Handler(void) {
//    m_systick_cnt++; 
//}

//uint32_t GetSysTick()
//{
//	return m_systick_cnt;
//}
/**@brief Application main function.
 */
float temperature;
float humidity;

double voltage;
//uint16_t rawAngle;
struct tm timeinfo;
volatile int gg;
int main(void)
{
    bool erase_bonds = false;
    // Initialize.

		//SCB->SCR |= SCB_SCR_SLEEPDEEP_Msk;
		//ggg = NRF_UICR->NRFFW[0];
		//NVIC_SetPriority(FPU_IRQn, APP_IRQ_PRIORITY_LOW);
		//NVIC_EnableIRQ(FPU_IRQn);
    log_init();
		//SEGGER_RTT_Write(
		
		NRF_LOG_INFO("Log started");
		NRF_LOG_PROCESS();
		GpioInit();

		//SysTick_Config(64000);
    //NVIC_EnableIRQ(SysTick_IRQn);
		__enable_irq();
		
    //timers_init();
		APP_ERROR_CHECK(nrf_drv_clock_init());
		nrf_drv_clock_lfclk_request(NULL);
    
		//CheckFwUpdate();
    
		//portSUPPRESS_TICKS_AND_SLEEP(1000);

    ble_stack_init();
		
		SpiInit();
		
		uint8_t res = ReadAirDeviceConfigFromEEPROM(&AirDeviceConfig);
		if(res != 0)
		{
			while(1);
		}
		
		TimersInit();

		AdcInit();
//		while(1)
//		{
//			sd_app_evt_wait() ;
//		}
		PwmInit();
		TwiInit();
	  FstorageInit();

		power_management_init();

		//At25EnterUltraDeepSleep();

		OsInit();
    gap_params_init();
    gatt_init();
    services_init();
    advertising_init();
		scan_init();
    conn_params_init();
		//db_discovery_init();
		
		//GetCurrentDateTime(&timeinfo);
		//scanning_start(&erase_bonds);

		advertising_start();
    // Start execution.
		//StartMotor(false);
//		StartMotor(true);
//		while(1){};
    //printf("\r\nUART started.\r\n");
    NRF_LOG_INFO("Debug logging for UART over RTT started.");

//    if(GetTemperatureAndHumidity(&temperature,&humidity) != NRF_SUCCESS)
//		{
//			while(1);
//		}
		
//		if(GetRowAngle(&rawAngle) != NRF_SUCCESS)
//		{
//			while(1);
//		}
			
		//nrf_delay_ms(1000);

		//GetRowAngle(&rowAngle);
		vTaskStartScheduler();
    // Enter main loop.
    for (;;)
    {
        
    }		
}

//void vApplicationIdleHook(void)
//{
//	do{
//                    __WFE();
//                } while (0 == (NVIC->ISPR[0] | NVIC->ISPR[1]));
//}

//#define FPU_EXCEPTION_MASK 0x0000009F

//void FPU_IRQHandler(void)
//{
//    uint32_t *fpscr = (uint32_t *)(FPU->FPCAR+0x40);
//    (void)__get_FPSCR();

//    *fpscr = *fpscr & ~(FPU_EXCEPTION_MASK);
//}


/**
 * @}
 */
